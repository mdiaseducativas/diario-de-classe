import Proxy = puremvc.Proxy;
import IProxy = puremvc.IProxy;

export class AppProxy extends Proxy implements IProxy {

    public static NAME:string = "AppProxy.NAME";

    // public authService: AuthenticationService;

    constructor(data?: any) {
        super(AppProxy.NAME, data);
    }

}