import ICommand = puremvc.ICommand;
import IFacade = puremvc.IFacade;

import {AppNotifications} from "../../notifications/app-notifications";
import {AppFacade} from "../../../../shared/core/application-facade";

export class AppCommand extends puremvc.SimpleCommand implements ICommand {

    /** @override */
    public execute(notification): void {

        let facade:IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);

        switch(notification.getName()) {

            case AppNotifications.REGISTER_MEDIATOR: {
                facade.registerMediator(notification.getBody());
                break;
            }

        }
    }
}
