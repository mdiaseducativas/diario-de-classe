import IFacade = puremvc.IFacade;
import ICommand = puremvc.ICommand;

import {AppCommand} from "./app-command";
import {AppFacade} from "../../../../shared/core/application-facade";
import {AppProxy} from "../../models/app-proxy";
import {StatusBarCommand} from '../../../../common/status-bar/shared/command/status-bar.command';
import {AppNotifications} from "../../notifications/app-notifications";
import {ConnectionNotifications} from "../../../../common/connection/shared/notifications/connection.notifications";
import {ConnectionCommand} from "../../../../common/connection/shared/controllers/command/connection.command";
import {StatusBarNotifications} from "../../../../common/status-bar/shared/notifications/status-bar.notifications";

export class StartCommand extends puremvc.SimpleCommand implements ICommand {

    /** @override */
    public execute(notification): void {

        switch (notification.getName()) {

            case AppFacade.APP_STARTUP: {

                console.log('AppFacade.APP_STARTUP');

                let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
                let appProxy: AppProxy = new AppProxy();
                facade.registerProxy(appProxy);

                facade.registerCommand(AppNotifications.REGISTER_MEDIATOR, AppCommand);
                facade.registerCommand(ConnectionNotifications.STARTUP_CONNECTION, ConnectionCommand);
                facade.registerCommand(StatusBarNotifications.CHANGE_TO_BLACK, StatusBarCommand);
                facade.registerCommand(StatusBarNotifications.CHANGE_TO_WHITE, StatusBarCommand);
                facade.sendNotification(ConnectionNotifications.STARTUP_CONNECTION);

                break;

            }

        }
    }
}
