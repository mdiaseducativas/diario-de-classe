import {Component} from '@angular/core';
import {Splashscreen, StatusBar} from 'ionic-native';
import {Platform} from "ionic-angular";

import {LoginPage} from "../../modules/login/page/login.page";

@Component({
    templateUrl: 'app.html'
})

export class Marituba {

    public rootPage: any = LoginPage;

    constructor(platform: Platform) {

        platform.ready().then(() => {

            Splashscreen.show();

            let cordova: any = window['cordova'];
            if (cordova && cordova.plugins['Keyboard']) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
            }

            if (platform.is('android')) {
                StatusBar.styleDefault();
            } else {
                StatusBar.overlaysWebView(true);
                StatusBar.backgroundColorByHexString('#ffffff');
            }

            setTimeout(() => Splashscreen.hide(), 500);

        });

    }

}
