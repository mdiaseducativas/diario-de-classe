import {AlertController, IonicApp, IonicErrorHandler, IonicModule, Platform, ToastController} from 'ionic-angular';
import {ErrorHandler, NgModule} from '@angular/core';
import {Storage} from "@ionic/storage";

import {Marituba} from './app.component';
import {Authentication} from "../../common/authentication/shared/services/authentication.service";
import {ConfigService} from "../../common/config/shared/services/config.service";
import {SharedModule} from "../../components/shared.module";
import {IonicEssentials} from '../../shared/model/ionic-essentials/ionic-essentials.model';
import {LoginModule} from "../../modules/login/login.module";
import {UsuarioModule} from "../../modules/usuario/usuario.module";
import {AnoLetivoModule} from "../../modules/ano-letivo/ano-letivo.module";
import {ConfigModule} from "../../common/config/config.module";
import {EscolaModule} from "../../modules/escola/escola.module";
import {HomeModule} from "../../modules/home/home.module";
import {FrequenciaModule} from "../../modules/frequencia/frequencia.module";
import {RegistroConteudoModule} from "../../modules/registro-conteudo/registro-conteudo.module";
import {LancamentoNotasModule} from "../../modules/lancamento-notas/lancamento-notas.module";

@NgModule({
    declarations: [
        Marituba
    ],
    imports: [
        LoginModule,
        EscolaModule,
        UsuarioModule,
        AnoLetivoModule,
        ConfigModule,
        HomeModule,
        RegistroConteudoModule,
        FrequenciaModule,
        LancamentoNotasModule,
        SharedModule,
        IonicModule.forRoot(Marituba, {
            backButtonText: ''
        })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        Marituba
    ],
    providers: [
        Authentication,
        ConfigService,
        Storage,
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        }]
})

export class AppModule {

    constructor(platform: Platform, storage: Storage, alertController: AlertController, toastController: ToastController) {

        IonicEssentials.platform = platform;
        IonicEssentials.storage = storage;
        IonicEssentials.alert.alertController = alertController;
        IonicEssentials.toast.toastController = toastController;

    }

}
