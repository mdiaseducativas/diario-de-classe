import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from './main/app.module';
import {AppFacade} from "../shared/core/application-facade";

let facade: AppFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
facade.startup();

setTimeout(() => {
    platformBrowserDynamic().bootstrapModule(AppModule);
}, 1000);
