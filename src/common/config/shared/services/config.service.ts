import {Injectable} from '@angular/core';
import {Http} from "@angular/http";

@Injectable()
export class ConfigService {

    private http: Http;
    private hostAuthentication: string;
    private host: string;
    private version: string;
    private prefix: string;
    private filePath: string;
    private escola: string;

    constructor(http: Http) {
        this.http = http;
        this.hostAuthentication = "";
        this.host = "";
        this.version = "";
        this.prefix = "";
        this.filePath = "";
        this.escola = "";
    }

    public load(): Promise<Object> {
        return new Promise((resolve) => {
            this.http
                .get('assets/config/config.json')
                .map(res => res.json())
                .subscribe(
                    data => this.onSuccessLoad(data, resolve),
                    response => this.onFailureGetConfig(resolve)
                );
        });
    }

    public onSuccessLoad(data, resolve): void {

        this.hostAuthentication = data.host_authentication;

        resolve();

    }

    public onFailureGetConfig(resolve: any): void {

        console.error("Erro ao carregar arquivo de configuração!");

        resolve();

    }

    public getHostAuthentication(): string {

        return this.hostAuthentication;

    }

    public getURL(): string {

        return this.host;

    }

}
