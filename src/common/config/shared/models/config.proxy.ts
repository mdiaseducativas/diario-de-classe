import IProxy = puremvc.IProxy;
import Proxy = puremvc.Proxy;
import 'rxjs/add/operator/map';

import {ConfigService} from "../services/config.service";

export class ConfigProxy extends Proxy implements IProxy {

    public static NAME: string = "ConfigProxy.NAME";

    public service: ConfigService;

    constructor(data?: any) {

        super(ConfigProxy.NAME, data);

    }

    public loadConfig(): Promise<Object> {

        return this.service.load();

    }

}
