import SimpleCommand = puremvc.SimpleCommand;
import IFacade = puremvc.IFacade;
import ICommand = puremvc.ICommand;
import INotification = puremvc.INotification;

import {AppFacade} from "../../../../../shared/core/application-facade";
import {ConfigProxy} from "../../models/config.proxy";
import {ConfigNotifications} from "../../notifications/config.notifications";

export class ReadConfigCommand extends SimpleCommand implements ICommand {

    /** @override */
    public execute(notification: INotification): void {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
        let proxy: ConfigProxy = facade.retrieveProxy(ConfigProxy.NAME) as ConfigProxy;
        let promise: Promise<Object> = proxy.loadConfig();

        promise
            .then(() => this.sendNotification(ConfigNotifications.SUCCESS_READ_CONFIG))
            .catch(() => this.sendNotification(ConfigNotifications.FAILURE_READ_CONFIG));

    }

}
