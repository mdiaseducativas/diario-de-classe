import IFacade = puremvc.IFacade;
import {NgModule} from '@angular/core';
import {IonicApp} from 'ionic-angular';

import {AppFacade} from "../../shared/core/application-facade";
import {ConfigService} from "./shared/services/config.service";
import {ConfigProxy} from "./shared/models/config.proxy";
import {ConfigNotifications} from "./shared/notifications/config.notifications";
import {ReadConfigCommand} from "./shared/controllers/command/read-config.command";

@NgModule({
    declarations: [],
    imports: [],
    bootstrap: [IonicApp],
    entryComponents: [],
    providers: [ConfigService]
})

export class ConfigModule {

    constructor(configService: ConfigService) {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
        let configProxy: ConfigProxy = new ConfigProxy();
        configProxy.service = configService;

        /** Register Proxy */
        facade.registerProxy(configProxy);

        /** Register Commands */
        facade.registerCommand(ConfigNotifications.READ_CONFIG, ReadConfigCommand);

    }

}
