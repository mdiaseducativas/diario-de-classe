import SimpleCommand = puremvc.SimpleCommand;
import ICommand = puremvc.ICommand;

import { StatusBarNotifications } from "../notifications/status-bar.notifications";
import { StatusBar } from 'ionic-native';

declare var cordova: any;

export class StatusBarCommand extends SimpleCommand implements ICommand {

    public execute(notification): void {

        switch (notification.getName()) {

            case StatusBarNotifications.CHANGE_TO_WHITE: {
                this.onChangeToWhite();
                break;
            }

            case StatusBarNotifications.CHANGE_TO_BLACK: {
                this.onChangeToBlack();
                break;
            }

        }
    }

    private onChangeToWhite(): void {

        if(!window['cordova']) {
            return;
        }

        if(cordova.platformId == 'ios') {
            StatusBar.styleLightContent();
        }

        // if(cordova.platformId == 'android') {
        //     StatusBar.backgroundColorByHexString("#ffffff");
        // }

    }

    private onChangeToBlack(): void {

        if(!window['cordova']) {
            return;
        }

        if(cordova.platformId == 'ios') {
            StatusBar.styleDefault();
        }

        // if(cordova.platformId == 'android') {
        //     StatusBar.backgroundColorByHexString("#000000");
        // }

    }

}