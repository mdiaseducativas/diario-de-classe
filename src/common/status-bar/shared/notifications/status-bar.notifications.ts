export class StatusBarNotifications {

    public static CHANGE_TO_WHITE: string = 'StatusBarNotifications.CHANGE_TO_WHITE';
    public static CHANGE_TO_BLACK: string = 'StatusBarNotifications.CHANGE_TO_BLACK';

}