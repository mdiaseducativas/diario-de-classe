export class PerfilConstants {

    public static ALUNO: number = 1;
    public static RESPONSAVEL: number = 2;
    public static ADMIN: number = 3;
    public static MASTER: number = 4;
    public static SUPORTE: number = 5;
    public static PROFESSOR: number = 6;
    public static COORDENADOR: number = 7;
    public static DIRETOR: number = 8;
    public static GESTOR: number = 9;

}