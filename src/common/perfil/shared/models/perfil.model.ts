import {Model} from '../../../../shared/model/base.model';

export class Perfil extends Model {

    private _id_perfil: number;
    private _nome: string;

    constructor(data:Object = {}) {
        super();
        this.set(data);
    }

    get id_perfil():number {
        return this._id_perfil;
    }

    set id_perfil(value:number) {
        this._id_perfil = value;
    }

    get nome():string {
        return this._nome;
    }

    set nome(value:string) {
        this._nome = value;
    }

    get toJSON(): Object {

        let object: Object = {
            "id_perfil": this.id_perfil,
            "nome": this.nome
        };

        return object;

    }

}
