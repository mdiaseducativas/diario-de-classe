import {Injectable} from '@angular/core';
import {Headers, Http, Response, URLSearchParams} from '@angular/http';
import {Observable} from "rxjs/Rx";

import {ConfigService} from '../../../config/shared/services/config.service';
import {EscolaFiltro} from "../../../../modules/escola/shared/models/escola-filtro.model";
import {Usuario} from '../../../../shared/model/index';

@Injectable()
export class Authentication {

    private http: Http;
    private headers: Headers;
    private config: ConfigService;

    constructor(http: Http, config: ConfigService) {

        this.http = http;
        this.config = config;
        this.headers = new Headers();
        this.headers.append('Content-Type', "application/json");

    }

    public loginAuthentication(usuario: Usuario): Observable<Response> {

        const body = new URLSearchParams();
        body.set('cpf', usuario.cpf);
        body.set('email', usuario.email);
        body.set('password', usuario.senha);

        const url: string = this.config.getHostAuthentication() + '/diarioclasse/login';
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        headers.append('cache-control', 'no-cache');

        return this.http.post(url, body, {headers});

    }

    public loginSchool(escolaFiltro: EscolaFiltro): Observable<Response> {

        // let url: string = this.config.getURL() + '/login';
        // let token: string = this.getAuthenticationToken();
        //
        // let headers = new Headers();
        // headers.append('Content-Type', "application/json");
        // headers.append('Authorization', 'Bearer ' + token);
        //
        // let payload: string = JSON.stringify({
        //     data: {
        //         escola: escola
        //     }
        // });
        //
        // return this.http.post(url, payload, {
        //     headers: headers
        // });

        return this.http.get('assets/data/login-escola.retorno.json');

    }

    public logout(): void {

        localStorage.clear();
        sessionStorage.clear();

    }

    public setAuthenticationToken(token: string): void {

        sessionStorage.setItem('authentication-token', token);

    }

    public getAuthenticationToken(): string {

        return sessionStorage.getItem('authentication-token');

    }

    public setSchoolToken(token: string): void {

        sessionStorage.setItem('school-token', token);

    }

    public getSchoolToken(): string {

        return sessionStorage.getItem('school-token');

    }

    public isLogged(): boolean {

        if (this.getAuthenticationToken()) {
            return true;
        }

        return false;

    }

}
