import SimpleCommand = puremvc.SimpleCommand;
import IFacade = puremvc.IFacade;
import ICommand = puremvc.ICommand;
import { ConnectionNotifications } from "../../notifications/connection.notifications";
import { Network } from 'ionic-native';
import { AppFacade } from "../../../../../shared/core/application-facade";
import { ConnectionConstants } from "../../constants/connection.constants";

declare var navigator: any;

export class ConnectionCommand extends SimpleCommand implements ICommand {

    private connectSubscription: any;
    private disconnectSubscription: any;

    execute(notification) {

        switch (notification.getName()) {

            case ConnectionNotifications.STARTUP_CONNECTION: {
                this.registerAll();
                this.startObservables();
                break;
            }

            case ConnectionNotifications.STOP_WATCH: {
                this.stopWatch();
                break;
            }

            case ConnectionNotifications.GET_CONNECTION_MODE: {
                this.getConnectionMode();
                break;
            }

        }

    }

    private startObservables(): void {

        this.connectSubscription = Network.onConnect().subscribe(() => {
            this.sendNotification(ConnectionNotifications.ONLINE_MODE);
        });

        this.disconnectSubscription = Network.onDisconnect().subscribe(() => this.sendNotification(ConnectionNotifications.OFFLINE_MODE));

    }

    private stopWatch(): void {

        this.connectSubscription.unsubscribe();
        this.disconnectSubscription.unsubscribe();

    }

    private getConnectionMode(): void {

        if (ConnectionCommand.isOnline())
            return this.sendNotification(ConnectionNotifications.ONLINE_MODE);

        this.sendNotification(ConnectionNotifications.OFFLINE_MODE);

    }

    public static isOnline(): boolean {

        if ((navigator.connection && navigator.connection.type == ConnectionConstants.NONE) || !navigator.onLine) {
            return false;
        }

        return true;

    }

    public registerAll(): void {

        this.registerCommand(ConnectionNotifications.STOP_WATCH, ConnectionCommand);
        this.registerCommand(ConnectionNotifications.GET_CONNECTION_MODE, ConnectionCommand);

    }

    private registerCommand(notificationName: string, CommandClass: any): void {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
        if (!facade.hasCommand(notificationName)) {
            facade.registerCommand(notificationName, CommandClass);
        }

    }

}
