export class ConnectionNotifications {

    public static STARTUP_CONNECTION: string = "ConnectionNotifications.STARTUP_CONNECTION";
    public static STOP_WATCH: string = "ConnectionNotifications.STOP_WATCH";

    public static GET_CONNECTION_MODE: string = "ConnectionNotifications.GET_CONNECTION_MODE";
    public static RESULT_GET_CONNECTION_MODE: string = "ConnectionNotifications.RESULT_GET_CONNECTION_MODE"

    public static OFFLINE_MODE: string = "ConnectionNotifications.OFFLINE_MODE";
    public static ONLINE_MODE: string = "ConnectionNotifications.ONLINE_MODE";

}