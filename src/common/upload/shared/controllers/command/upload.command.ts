import SimpleCommand = puremvc.SimpleCommand;
import IFacade = puremvc.IFacade;
import ICommand = puremvc.ICommand;

import {UploadProxy} from "../../models/upload.proxy";
import {UploadNotifications} from "../../notifications/upload.notifications";
import {AppFacade} from "../../../../../shared/core/application-facade";

export class UploadCommand extends SimpleCommand implements ICommand {

    execute(notification) {

        let facade:IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);

        switch(notification.getName()) {

            case UploadNotifications.STARTUP_UPLOAD: {

                this.registerAll();
                break;

            }

            case UploadNotifications.SET_SERVICE: {

                if(!facade.hasProxy(UploadProxy.NAME)) {
                    let proxy: UploadProxy = new UploadProxy();
                    proxy.service = notification.getBody();
                    facade.registerProxy(proxy);
                }else {
                    let proxy: UploadProxy = facade.retrieveProxy(UploadProxy.NAME) as UploadProxy;
                    if(notification.getBody() != proxy.service) {
                        proxy.service = notification.getBody();
                    }
                }

                break;

            }

            case UploadNotifications.SAVE_FILE: {

                let proxy: UploadProxy = facade.retrieveProxy(UploadProxy.NAME) as UploadProxy;
                proxy.uploadFile(notification.getBody());

                break;

            }

        }
    }

    public registerAll(): void {

        this.registerCommand(UploadNotifications.SET_SERVICE, UploadCommand);
        this.registerCommand(UploadNotifications.SAVE_FILE, UploadCommand);

    }

    private registerCommand(notificationName:string, CommandClass:any): void {

        let facade:IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
        if(!facade.hasCommand(notificationName)) {
            facade.registerCommand(notificationName, CommandClass);
        }

    }

}
