import { Injectable }    from '@angular/core';
import 'rxjs/add/operator/map';

import {Observable} from "rxjs/Rx";
import {ConfigService} from "../../../config/shared/services/config.service";
import {Service} from "../../../../shared/services/base.service";

@Injectable()
export class UploadService extends Service {

    private config: ConfigService;

    constructor(config: ConfigService) {

        super();
        this.config = config;

    }

    public uploadFile(files: File[]): Observable<Object> {

        return Observable.create(observer => {

            let xhr: XMLHttpRequest = new XMLHttpRequest();
            let uri: string = this.config.getURL() + '/file';

            let formData: FormData = new FormData();

            let i: number = 0;
            let length: number = files.length;
            for(; i < length ; i++) {
                formData.append('newfile[]', files[i], files[i].name);
            }

            xhr.onreadystatechange = () => {

                if(xhr.readyState === 4) {
                    if(xhr.status === 200) {
                        observer.next(JSON.parse(xhr.response));
                        observer.complete();
                    }else {
                        observer.error(xhr.response);
                    }
                }

            };

            // xhr.upload.onprogress = (event) => {
            //
            //     let progress = Math.round(event.loaded / event.total * 100);
            //     //TODO: implementar progress
            //
            // };

            xhr.open('POST', uri, true);
            xhr.setRequestHeader('Authorization','Bearer ' + this.getToken());
            xhr.send(formData);

        });

    }

}
