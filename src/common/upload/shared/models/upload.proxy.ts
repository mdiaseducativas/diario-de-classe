import IProxy = puremvc.IProxy;
import Proxy = puremvc.Proxy;

import {Observable} from "rxjs";
import 'rxjs/add/operator/map';

import {UploadService} from "../services/upload.service";
import {UploadNotifications} from "../notifications/upload.notifications";

export class UploadProxy extends Proxy implements IProxy {

    public static NAME: string = "UploadProxy.NAME";

    public service: UploadService;

    constructor(data?: any) {

        super(UploadProxy.NAME, data);

    }

    public uploadFile(files: File[]): void {

        try {

            let observer: Observable<Object> = this.service.uploadFile(files);
            observer.subscribe((response: Object) => this.sendNotification(UploadNotifications.SUCCESS_SAVE_FILE, response['materiais']),
                               (response: Object) => this.onFailureSaveFile(response));

        }catch(err) {
            console.log(err);
        }

    }

    public onFailureSaveFile(response: Object) {

        console.log('onFailureSaveFile', response);
        // this.sendNotification(UploadNotifications.FAILURE_SAVE_FILE, response)

    }

}
