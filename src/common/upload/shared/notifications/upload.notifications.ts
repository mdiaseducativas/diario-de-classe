export class UploadNotifications {

    public static STARTUP_UPLOAD: string = "UploadNotifications.STARTUP_UPLOAD";
    public static SET_SERVICE: string = "UploadNotifications.SET_SERVICE";

    public static SAVE_FILE: string = "UploadNotifications.SAVE_FILE";
    public static SUCCESS_SAVE_FILE: string = "UploadNotifications.SUCCESS_SAVE_FILE";
    public static FAILURE_SAVE_FILE: string = "UploadNotifications.FAILURE_SAVE_FILE";

}
