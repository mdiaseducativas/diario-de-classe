import {IonicModule} from "ionic-angular";
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

import {InteractionDirective} from "./interaction/interaction.directive";
import {LoadSpinComponent} from "./load-spin/load-spin.component";
import {SpinComponent} from "./spin/spin.component";

@NgModule({
    imports: [
        IonicModule,
        CommonModule
    ],
    providers: [],
    declarations: [
        LoadSpinComponent,
        InteractionDirective,
        SpinComponent
    ],
    entryComponents: [],
    exports: [
        CommonModule,
        FormsModule,
        LoadSpinComponent,
        InteractionDirective,
        SpinComponent
    ]
})

export class SharedModule {

}
