import {Directive, Input, Output, EventEmitter, AfterViewInit} from '@angular/core';

@Directive({
    selector: '[interaction]'
})

export class InteractionDirective implements AfterViewInit {

    @Input() first: boolean;
    @Input() last: boolean;

    @Output() onFirst: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() onLast: EventEmitter<boolean> = new EventEmitter<boolean>();

    ngAfterViewInit() {
        this.check();
    }

    check() {
        if(this.last) {
            this.onLast.emit(true);
        }else if(this.first) {
            this.onFirst.emit(true);
        }
    }
}