export class FrequenciaConstants {

    public static PRESENCA: string = "PRESENCA";
    public static FALTA: string = "FALTA";
    public static FALTA_JUSTIFICADA: string = "FALTA_JUSTIFICADA";

}
