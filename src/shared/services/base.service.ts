import { Headers } from "@angular/http";

export class Service {

    public getHeaders(): Headers {

        let token: string = sessionStorage.getItem('gde-token');

        let headers: Headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + token);

        return headers;

    }

    public getToken(): string {

        return sessionStorage.getItem('gde-token');

    }

}