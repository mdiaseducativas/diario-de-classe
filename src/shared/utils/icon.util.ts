declare var toastr: any;

/**
 * Created by Ricardo Teixeira on 04/01/2017.
 */
export class IconUtil {

    constructor() {}

    public static getMimeTypeIcon(mimeType: string): Object {

        let iconClasses: Object = {
            'fa': true
        };

        if(mimeType.indexOf('pdf') >= 0) {
            iconClasses["fa-file-pdf-o"] = true;
        }else if(mimeType.indexOf('image') >= 0) {
            iconClasses["fa-picture-o"] = true;
        }else if(mimeType.indexOf('zip') >= 0) {
            iconClasses["fa-file-archive-o"] = true;
        }else if(mimeType.indexOf('excel') >= 0) {
            iconClasses["fa-file-excel-o"] = true;
        }else if(mimeType.indexOf('word') >= 0) {
            iconClasses["fa-file-word-o"] = true;
        }else {
            iconClasses["fa-file-o"] = true;
        }

        return iconClasses;

    }

    public static getIcon(url: string): Object {

        let extensao = url.split('.').pop();
        let iconClasses: Object = {
            'fa': true
        };

        if(extensao.indexOf('pdf') >= 0) {
            iconClasses["fa-file-pdf-o"] = true;
        }else if(extensao.indexOf('png') >= 0 || extensao.indexOf('jpg') >= 0) {
            iconClasses["fa-picture-o"] = true;
        }else if(extensao.indexOf('zip') >= 0) {
            iconClasses["fa-file-archive-o"] = true;
        }else if(extensao.indexOf('excel') >= 0) {
            iconClasses["fa-file-excel-o"] = true;
        }else if(extensao.indexOf('word') >= 0) {
            iconClasses["fa-file-word-o"] = true;
        }else {
            iconClasses["fa-file-o"] = true;
        }

        return iconClasses;

    }

}