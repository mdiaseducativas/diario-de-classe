export class Util {

    public static getLetraByIndex(index: number): string {

        var letra: any = "A";

        for (var i = 0; i < index; i++) {
            letra = String.fromCharCode(letra.charCodeAt() + 1);
        }

        return letra;

    }

    public static getExtensao(nome: string): string {

        return nome.substr(nome.lastIndexOf('.') + 1);

    }

    public static convertToBoolean(value: any): boolean {

        if (value == "true") {
            return true;
        } else if (value == "false") {
            return false;
        }

        if (value == "1") {
            return true;
        } else if (value == "0") {
            return false;
        }

        return Boolean(value);

    }

    public static getDateByTimestamp(timestamp: number): string {

        let data: Date = new Date(timestamp);
        let dia: string = (data.getDate() < 10) ? "0" + data.getDate() : String(data.getDate());
        let mes: string = (data.getMonth() < 10) ? "0" + data.getMonth() : String(data.getMonth());
        let ano: number = data.getFullYear();

        let hora: string = (data.getHours() < 10) ? "0" + data.getHours() : String(data.getHours());
        let minuto: string = (data.getMinutes() < 10) ? "0" + data.getMinutes() : String(data.getMinutes());

        return dia + "/" + mes + "/" + ano + " " + hora + ":" + minuto;

    }

    public static getMimeTypeURL(url: string): string {

        let extensao: string = url.split('.').pop().toLowerCase();

        if (extensao == "jpg" || extensao == "jpeg")
            return "image/jpeg";

        if (extensao == "png")
            return "image/png";

        if (extensao == "pdf")
            return "application/pdf";

        if (extensao == "mp3")
            return "audio/mp3";

        return "";

    }

}
