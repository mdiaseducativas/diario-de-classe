import {Response} from '@angular/http';
import {AlertController} from "ionic-angular";

export class Alert {

    public alertController: AlertController;

    public basic(response: Response, title: string = '', subtitle: string = ''): void {

        if (response) {
            let data = response.json() || {};
            if (data.message) {
                subtitle = data.message;
            }
        }

        let alert = this.alertController.create({
            title: title,
            subTitle: subtitle,
            buttons: ['Dismiss']
        });

        alert.present();

    }

}
