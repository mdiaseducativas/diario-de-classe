import {ToastController} from "ionic-angular";

export class ToastUtil {

    public toastController: ToastController;

    public showToast(mensagem: string, duration: number = 3000, position: string = 'bottom') {

        let toast = this.toastController.create({
            message: mensagem,
            duration: duration,
            position: position
        });

        toast.present();

    }

}
