export class ConvertDateDBUtil {

    constructor() {
    }

    public static removeHours(date: string): string {

        return date.split(" ")[0];

    }

    public static getHoursFromDate(date: Date): string {

        let hours = date.getHours();
        let minutes = date.getMinutes() <= 9 ? '0' + date.getMinutes() : date.getMinutes();

        return hours + ':' + minutes;

    }

    public static getHoursFromDateDB(date: string) {

        let arrDate = date.split(" ");

        let hours: string = '';
        let minutes: string = '';

        if (arrDate[1]) {
            let arrHours = arrDate[1].split(":");
            hours = arrHours[0] + 'h';
            minutes = arrHours[1];
        }

        return hours + minutes;

    }

    public static convertDateToDB(date: string): string {

        let myDate: any = date;
        let data: any;
        let hora: any;

        myDate = myDate.split(" ");

        data = myDate[0];
        hora = myDate[1];

        data = data.split("/");
        if (data.length == 1) {
            return data;
        }

        data = data[2] + "-" + data[1] + "-" + data[0];

        if (hora) {
            data += " " + hora;
        }

        return data;

    }

    public static convertDateFromDB(date: string, returnHours: boolean = true): string {

        let arrData: string[];
        let data: any;
        let hora: any;
        let newDate: any;

        arrData = date.split(" ");

        data = arrData[0];
        hora = arrData[1];
        data = data.split("-");

        if (data.length == 1) {
            return date;
        }

        data = data[2] + "/" + data[1] + "/" + data[0];

        newDate = data;

        if (returnHours) {
            newDate += " " + (hora || "");
        }

        return newDate;

    }

    public static convertFormatedDateToDate(date: string): Date {

        let arrDate = date.split(" ");
        let hora = arrDate[1];
        let data: any = arrDate[0];

        data = data.split("/");
        data = data[1] + "/" + data[0] + "/" + data[2];

        if (hora) {
            data = data + " " + hora;
        }

        return new Date(data);

    }

    public static convertDateToString(date: Date, showHour = false): string {

        let ano = date.getFullYear();
        let mes = (date.getMonth() + 1) <= 9 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);
        let dia = date.getDate() <= 9 ? '0' + date.getDate() : date.getDate();

        let data = ano + '-' + mes + '-' + dia;
        if (showHour) {
            let hora = date.getHours();
            let minutos = date.getMinutes();

            data += ' ' + hora + ':' + minutos;
        }

        return data;

    }

    public static isToday(date: string): boolean {

        let convertedDate: string = this.convertDateFromDB(date);
        let newDate: Date = this.convertFormatedDateToDate(convertedDate);

        if (newDate.toDateString() == new Date().toDateString()) {
            return true;
        }

        return false;

    }

}
