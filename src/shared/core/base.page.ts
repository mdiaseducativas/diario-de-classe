import IMediator = puremvc.IMediator;
import Mediator = puremvc.Mediator;
import IFacade = puremvc.IFacade;
import {OnDestroy} from "@angular/core";

import {AppFacade} from "./application-facade";

export class Page extends Mediator implements IMediator, OnDestroy {

    private mediators: string[];

    constructor() {
        super();
        this.mediators = [];
    }

    protected addMediator(mediator: any) {

        this.mediators.push(mediator);

    }

    public ngOnDestroy(): void {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);

        this.mediators.forEach((mediator: any) => {

            mediator.removeAllSubscribers();

            if (facade.hasMediator(mediator.constructor.NAME)) {
                facade.removeMediator(mediator.constructor.NAME);
            }

        });

    }

}
