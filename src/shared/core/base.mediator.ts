import IMediator = puremvc.IMediator;
import Mediator = puremvc.Mediator;
import INotification = puremvc.INotification;
import {EventEmitter} from "@angular/core";
import {Subscriber} from 'rxjs/Rx';

import {AppFacade} from "./application-facade";
import {AppNotifications} from "../../app/main/notifications/app-notifications";

export class BaseMediator extends Mediator implements IMediator {

    private subscribers: Subscriber<any>[];

    protected listeners: any;

    constructor(mediatorName?: string) {

        super(mediatorName, null);

        this.listeners = {};

        this.subscribers = [];
        this.multitonKey = AppFacade.APP_STARTUP;

        /**
         * A função"registerListeners()" deve ser chamado antes do
         * REGISTER_MEDIATOR, pois adiciona todos os listeners na
         * variavel "listeners" que é usada na funcao
         * "listNotificationInterests()"
         * */

        this.registerListeners();

        this.sendNotification(AppNotifications.REGISTER_MEDIATOR, this);

    }

    /** @override */
    public listNotificationInterests(): string[] {

        let listNotifications: string[] = [];

        for (let key in this.listeners) {
            listNotifications.push(key);
        }

        return listNotifications;

    }

    /** @override */
    public handleNotification(notification: INotification): void {

        let notificationName: string = notification.getName();
        if (this.listeners[notificationName]) {
            this.listeners[notificationName](notification.getBody());
        }

    }

    public addListener(notificationName: string, listener: Function): void {

        this.listeners[notificationName] = listener.bind(this);

    }

    public registerListeners(): void {

    }

    protected subscribe(eventEmitter: EventEmitter<any>, listener: Function): void {

        let subscriber: Subscriber<any> = eventEmitter.subscribe(listener.bind(this));

        this.subscribers.push(subscriber);

    }

    public removeAllSubscribers(): void {

        this.subscribers.forEach((subscriber: Subscriber<any>) => subscriber.unsubscribe());

    }

}
