import {StartCommand} from "../../app/main/controllers/command/start-command";

export class AppFacade extends puremvc.Facade {

    public static APP_STARTUP:string = 'APP_STARTUP';

    /** @override */
    initializeController() {
        super.initializeController();
        this.registerCommand(AppFacade.APP_STARTUP, StartCommand);
    }

    /** @override */
    initializeModel() {
        super.initializeModel();
    }

    /** @override */
    initializeView() {
        super.initializeView();
    }

    startup (app:any = {}) {
        this.sendNotification(AppFacade.APP_STARTUP, app);
    }

    static getInstance(multitonKey?: string) {
        const instanceMap = puremvc.Facade.instanceMap;
        if (!instanceMap[AppFacade.APP_STARTUP]) {
            instanceMap[AppFacade.APP_STARTUP] = new AppFacade(AppFacade.APP_STARTUP);
        }
        return instanceMap[AppFacade.APP_STARTUP];
    }
}
