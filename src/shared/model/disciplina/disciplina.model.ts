import {Model} from '../base.model';

export class Disciplina extends Model {

    id_disciplina: string;
    nome: string;

    constructor(data:Object = {}) {
        super();
        this.set(data);
    }

}
