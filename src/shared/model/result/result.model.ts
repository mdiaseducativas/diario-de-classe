import {Response} from '@angular/http';

import {Model} from "../base.model";

export class Result extends Model {

    links: any;
    data: any;
    response: Response;

    constructor(data: Object = {}) {
        super();
        this.set(data);
    }

}
