import {Platform} from 'ionic-angular';
import {Storage} from "@ionic/storage";

import {Alert} from "../../utils/alert.util";
import {ToastUtil} from "../../utils/toast.util";

export class IonicEssentials {

    public static platform: Platform;
    public static storage: Storage;
    public static alert: Alert = new Alert();
    public static toast: ToastUtil = new ToastUtil();

}
