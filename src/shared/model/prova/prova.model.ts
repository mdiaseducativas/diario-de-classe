import {Model} from "../base.model";

export class Prova extends Model {

    id_prova: string;
    nome: string;
    nota: string;

    constructor(data: Object = {}) {
        super();
        this.set(data);
    }

}
