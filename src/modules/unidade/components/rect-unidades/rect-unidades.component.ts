import {Component} from '@angular/core';

import {Unidade} from "../../shared/models/unidade.model";

@Component({
    selector: 'rect-unidades-component',
    templateUrl: 'rect-unidades.component.html',
    providers: []
})

export class RectUnidadesComponent {

    protected unidade: Unidade;
    protected unidades: Unidade[];

    constructor() {

        this.unidade = null;
        this.unidades = [];

    }

    public setUnidades(unidades: Unidade[]): void {

        this.unidades = unidades;

    }

    public setUnidade(unidade: Unidade): void {

        this.unidade = unidade;

    }

    public getUnidades(): Unidade[] {

        return this.unidades;

    }

    public getUnidade(): Unidade {

        return this.unidade;

    }

    protected escolherUnidade(): void {



    }

}
