import {Model} from "../../../../shared/model/base.model";
import {Prova} from "../../../../shared/model/prova/prova.model";

export class Unidade extends Model {

    id_unidade: string;
    nome: string;
    media: string;
    nota: string;
    provas: Prova[];

    constructor(data: Object = {}) {
        super();
        this.set(data);
    }

}
