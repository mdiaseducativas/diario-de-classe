import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {SharedModule} from "../../components/shared.module";
import {RectUnidadesComponent} from "./components/rect-unidades/rect-unidades.component";

@NgModule({
    declarations: [
        RectUnidadesComponent
    ],
    entryComponents: [
        RectUnidadesComponent
    ],
    exports: [
        RectUnidadesComponent
    ],
    imports: [
        IonicModule.forRoot(RectUnidadesComponent),
        SharedModule
    ],
    bootstrap: [IonicApp],
    providers: [
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        }
    ]
})

export class UnidadeModule {

}
