import {Component, EventEmitter, Output} from '@angular/core';

import {Escola} from "../../shared/models/escola.model";
import {Turma} from "../../shared/models/turma.model";

@Component({
    selector: 'turma-grid-component',
    templateUrl: 'turma-grid.component.html',
    providers: []
})

export class TurmaGridComponent {

    @Output() public onSelectTurma: EventEmitter<Turma>;

    protected turma: Turma;
    protected turmas: Turma[];

    constructor() {

        this.turmas = [];
        this.turma = new Turma();
        this.onSelectTurma = new EventEmitter<Turma>();

    }

    public setTurmas(turmas: Turma[]): void {

        this.turmas = turmas;

    }

    public setTurma(turma: Turma): void {

        this.turma = turma;

    }

    protected selectTurma(turma: Turma): void {

        this.setTurma(turma);

        this.onSelectTurma.emit(turma);

    }

}
