export class TurmaNotifications {

    public static READ_TURMAS: string = "EscolaNotifications.READ_TURMAS";
    public static SUCCESS_READ_TURMAS: string = "EscolaNotifications.SUCCESS_READ_TURMAS";
    public static FAILURE_READ_TURMAS: string = "EscolaNotifications.FAILURE_READ_TURMAS";

}
