import {Turma} from "./turma.model";
import {Disciplina} from "../../../../shared/model/disciplina/disciplina.model";

export class TurmaFactory {

    public create(data: any): any {

        switch (data.constructor) {
            case Array: {
                return this.createMany(data);
            }

            case Object: {
                return this.createOne(data);
            }
        }

    }


    private createOne(data: Object): Turma {

        let turma: Turma = new Turma(data);

        if (turma.disciplina) {
            turma.disciplina = new Disciplina(turma.disciplina);
        }

        return turma;

    }

    private createMany(data: Object[]): Turma[] {

        let turmas: Turma[] = [];

        for (let i: number = 0; i < data.length; i++) {
            let turma: Turma = this.createOne(data[i]);
            turmas.push(turma);
        }

        return turmas;

    }

}
