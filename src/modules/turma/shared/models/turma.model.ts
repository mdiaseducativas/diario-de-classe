import {Escola} from '../../../escola/shared/models/escola.model';
import {Model} from "../../../../shared/model/base.model";
import {Disciplina} from "../../../../shared/model/disciplina/disciplina.model";
import {Horario} from "../../../horarios/shared/models/horario.model";

export class Turma extends Model {

    id_turma: string;
    nome: string;
    escola: Escola;
    disciplina: Disciplina;
    horarios: Horario[];

    constructor(data: Object = {}) {
        super();
        this.disciplina = new Disciplina();
        this.set(data);
    }

}
