import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler} from 'ionic-angular';

import {TurmaGridComponent} from "./components/turma-grid/turma-grid.component";
import {SharedModule} from "../../components/shared.module";

@NgModule({
    declarations: [
        TurmaGridComponent
    ],
    entryComponents: [
        TurmaGridComponent
    ],
    exports: [
        TurmaGridComponent
    ],
    imports: [
        SharedModule
    ],
    bootstrap: [IonicApp],
    providers: [
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        }
    ]
})

export class TurmaModule {

    constructor() {}

}
