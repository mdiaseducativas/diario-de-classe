import {Model} from '../../../../shared/model/base.model';

export class HomeMenuItem extends Model {

    nome: string;
    icone: string;
    page: string;
    iconClass: string;

    constructor(data:Object = {}) {
        super();
        this.set(data);
    }

}
