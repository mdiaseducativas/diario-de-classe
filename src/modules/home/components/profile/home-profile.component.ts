import {Component, EventEmitter, Output} from '@angular/core';

import {Usuario} from "../../../usuario/shared/models/usuario.model";

@Component({
    selector: 'home-profile-component',
    templateUrl: 'home-profile.component.html',
    providers: []
})

export class HomeProfileComponent {

    @Output() onLogout: EventEmitter<null>;

    protected usuario: Usuario;

    constructor() {

        this.usuario = new Usuario();
        this.onLogout = new EventEmitter<null>();

    }

    public setUsuario(usuario: Usuario): void {

        this.usuario = usuario;

    }

    protected logout(): void {

        this.onLogout.emit();

    }

}
