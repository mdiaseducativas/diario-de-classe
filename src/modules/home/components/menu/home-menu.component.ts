import {Component, EventEmitter, Output} from '@angular/core';

import {HomeMenuItem} from "../../shared/models/home-menu-item.model";
import {FrequenciaPage} from "../../../frequencia/page/frequencia.page";
import {RegistroConteudoPage} from "../../../registro-conteudo/page/registro-conteudo.page";
import {LancamentoNotasPage} from "../../../lancamento-notas/pages/lancamento-notas/lancamento-notas.page";

@Component({
    selector: 'home-menu-component',
    templateUrl: 'home-menu.component.html',
    providers: []
})

export class HomeMenuComponent {

    @Output() public onSelectMenuItem: EventEmitter<HomeMenuItem>;

    protected menuItems: HomeMenuItem[];

    constructor() {

        this.menuItems = [];

        this.onSelectMenuItem = new EventEmitter<HomeMenuItem>();

        this.addMenus();

    }

    private addMenus(): void {

        let frequencia: HomeMenuItem = new HomeMenuItem({
            nome: 'Frequência',
            icone: 'assets/icon/icon-frequencia.png',
            iconClass: 'icon-frequencia',
            page: FrequenciaPage
        });

        let registroConteudo: HomeMenuItem = new HomeMenuItem({
            nome: 'Registro de Conteúdo',
            icone: 'assets/icon/icon-registro-conteudo.png',
            iconClass: 'icon-lancamento-notas',
            page: RegistroConteudoPage
        });

        let lancamentoNotas: HomeMenuItem = new HomeMenuItem({
            nome: 'Lançamento de Notas',
            icone: 'assets/icon/icon-lancamento-notas.png',
            iconClass: 'icon-registro-conteudo',
            page: LancamentoNotasPage
        });

        this.menuItems.push(frequencia);
        this.menuItems.push(registroConteudo);
        this.menuItems.push(lancamentoNotas);

    }

    public selectMenuItem(menuItem: HomeMenuItem): void {

        this.onSelectMenuItem.emit(menuItem);

    }

}
