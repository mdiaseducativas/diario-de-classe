import {BaseMediator} from "../../../../../shared/core/base.mediator";
import {HomePage} from "../../home.page";
import {HomeMenuItem} from "../../../shared/models/home-menu-item.model";
import {NavController} from "ionic-angular";

export class HomeMenuMediator extends BaseMediator {

    public static NAME: string = 'HomeMenuMediator.NAME';

    private navController: NavController;

    constructor(homePage: HomePage) {

        super(HomeMenuMediator.NAME);

        this.navController = homePage.navController;

        this.subscribe(homePage.homeMenuComponent.onSelectMenuItem, this.onSelectMenuItem);

    }

    private onSelectMenuItem(menuItem: HomeMenuItem): void {

        this.navController.push(menuItem.page);

    }

}
