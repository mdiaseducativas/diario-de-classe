import {BaseMediator} from "../../../../../shared/core/base.mediator";
import {HomePage} from "../../home.page";
import {EscolaRectComponent} from "../../../../escola/components/rect/escola-rect.component";
import {Escola} from "../../../../escola/shared/models/escola.model";
import {EscolaStorageNotifications} from "../../../../escola/shared/notifications/escola-storage.notifications";
import {EscolaFiltro} from "../../../../escola/shared/models/escola-filtro.model";
import {Turma} from "../../../../turma/shared/models/turma.model";
import {AnoLetivo} from "../../../../ano-letivo/shared/models/ano-letivo.model";
import {NavController} from "ionic-angular";
import {EscolaPage} from "../../../../escola/page/escola.page";

export class HomeEscolaMediator extends BaseMediator {

    public static NAME: string = 'HomeEscolaMediator.NAME';

    protected homePage: HomePage;
    protected escolaRectComponent: EscolaRectComponent;
    protected navController: NavController;

    constructor(homePage: HomePage) {

        super(HomeEscolaMediator.NAME);

        this.homePage = homePage;
        this.navController = homePage.navController;
        this.escolaRectComponent = homePage.escolaRectComponent;

        this.subscribe(this.escolaRectComponent.onClick, this.onClickEscola);

        this.readEscolaFiltro();

    }

    private readEscolaFiltro(): void {

        this.sendNotification(EscolaStorageNotifications.READ_ESCOLA_FILTRO);

    }

    private onClickEscola(): void {

        this.sendNotification(EscolaStorageNotifications.DELETE_ESCOLA_FILTRO);

        this.navController.setRoot(EscolaPage);

    }

    /** @override */
    public registerListeners(): void {

        this.addListener(EscolaStorageNotifications.SUCCESS_READ_ESCOLA_FILTRO, this.onSuccessReadEscolaFiltro);
        this.addListener(EscolaStorageNotifications.FAILURE_READ_ESCOLA_FILTRO, this.onFailureReadEscolaFiltro);

    }

    private onSuccessReadEscolaFiltro(escolaFiltro: EscolaFiltro): void {

        if(!escolaFiltro.isValid()) {
            return;
        }

        let escola: Escola = escolaFiltro.escola;
        let turma: Turma = escolaFiltro.turma;
        let anoLetivo: AnoLetivo = escolaFiltro.anoLetivo;

        this.escolaRectComponent.setEscola(escola);
        this.escolaRectComponent.setTurma(turma);
        this.escolaRectComponent.setAnoLetivo(anoLetivo);

    }

    private onFailureReadEscolaFiltro(): void {

    }

}
