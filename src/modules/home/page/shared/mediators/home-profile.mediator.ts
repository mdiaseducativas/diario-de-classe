import {BaseMediator} from "../../../../../shared/core/base.mediator";
import {HomePage} from "../../home.page";
import {HomeProfileComponent} from "../../../components/profile/home-profile.component";
import {UsuarioStorageNotifications} from "../../../../usuario/shared/notifications/usuario-storage.notifications";
import {Usuario} from "../../../../usuario/shared/models/usuario.model";
import {LoginNotifications} from "../../../../login/shared/notifications/login.notification";
import {NavController} from "ionic-angular";
import {LoginPage} from "../../../../login/page/login.page";

export class HomeProfileMediator extends BaseMediator {

    public static NAME: string = 'HomeProfileMediator.NAME';

    protected homePage: HomePage;
    protected homeProfileComponent: HomeProfileComponent;
    protected navController: NavController;

    constructor(homePage: HomePage) {

        super(HomeProfileMediator.NAME);

        this.homePage = homePage;
        this.navController = homePage.navController;
        this.homeProfileComponent = homePage.homeProfileComponent;

        this.sendNotification(UsuarioStorageNotifications.READ_USUARIO);

        this.subscribe(this.homeProfileComponent.onLogout, this.logout);

    }

    private logout(): void {

        this.sendNotification(LoginNotifications.LOGOUT);

        this.navController.setRoot(LoginPage);

    }

    /** @override */
    public registerListeners(): void {

        this.addListener(UsuarioStorageNotifications.SUCCESS_READ_USUARIO, this.onSuccessReadUsuario);
        this.addListener(UsuarioStorageNotifications.FAILURE_READ_USUARIO, this.onFailureReadUsuario);

    }

    private onSuccessReadUsuario(usuario: Usuario): void {

        this.homeProfileComponent.setUsuario(usuario);

    }

    private onFailureReadUsuario(): void {

    }

}
