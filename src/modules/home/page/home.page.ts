import {Component, OnInit, ViewChild} from '@angular/core';

import {EscolaRectComponent} from "../../escola/components/rect/escola-rect.component";
import {HomeProfileComponent} from "../components/profile/home-profile.component";
import {HomeMenuComponent} from "../components/menu/home-menu.component";
import {HomeEscolaMediator} from "./shared/mediators/home-escola.mediator";
import {Page} from "../../../shared/core/base.page";
import {HomeProfileMediator} from "./shared/mediators/home-profile.mediator";
import {HomeMenuMediator} from "./shared/mediators/home-menu.mediator";
import {NavController} from "ionic-angular";

@Component({
    selector: 'home-page',
    templateUrl: 'home.page.html',
    providers: []
})

export class HomePage extends Page implements OnInit {

    @ViewChild(HomeProfileComponent) homeProfileComponent: HomeProfileComponent;
    @ViewChild(EscolaRectComponent) escolaRectComponent: EscolaRectComponent;
    @ViewChild(HomeMenuComponent) homeMenuComponent: HomeMenuComponent;

    public homeEscolaMediator: HomeEscolaMediator;
    public homeProfileMediator: HomeProfileMediator;
    public homeMenuMediator: HomeMenuMediator;
    public navController: NavController;

    protected loading: boolean;

    constructor(navController: NavController) {

        super();

        this.loading = true;

        this.navController = navController;

    }

    public setLoading(loading: boolean): void {

        this.loading = loading;

    }

    public ngOnInit() {

        this.homeEscolaMediator = new HomeEscolaMediator(this);
        this.homeProfileMediator = new HomeProfileMediator(this);
        this.homeMenuMediator = new HomeMenuMediator(this);

        this.addMediator(this.homeEscolaMediator);
        this.addMediator(this.homeProfileMediator);
        this.addMediator(this.homeMenuMediator);

    }

}
