import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {SharedModule} from "../../components/shared.module";
import {ConfigService} from "../../common/config/shared/services/config.service";
import {Authentication} from "../../common/authentication/shared/services/authentication.service";
import {LoginPage} from "./page/login.page";
import {LoginFormComponent} from "./components/login-form/login-form.component";
import {LoginProxy} from "./shared/models/login.proxy";
import {LoginNotifications} from "./shared/notifications/login.notification";
import {LoginCommand} from "./shared/controllers/commands/login.command";
import {LogoutCommand} from "./shared/controllers/commands/logout.command";
import {LoginEscolaCommand} from "./shared/controllers/commands/login-escola.command";
import {HomePage} from "./page/home.page";
import {HomeProfileComponent} from "./components/profile/home-profile.component";
import {HomeMenuComponent} from "./components/menu/home-menu.component";
import {EscolaModule} from "../escola/escola.module";

@NgModule({
    declarations: [
        HomePage,
        HomeProfileComponent,
        HomeMenuComponent
    ],
    imports: [
        IonicModule.forRoot(HomePage),
        SharedModule,
        EscolaModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        HomePage,
        HomeProfileComponent,
        HomeMenuComponent
    ],
    providers: [
        Authentication,
        ConfigService,
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        }
    ]
})

export class HomeModule {

}
