import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {SharedModule} from "../../components/shared.module";
import {EscolaModule} from "../escola/escola.module";
import {LancamentoNotasPage} from "./pages/lancamento-notas/lancamento-notas.page";
import {UsuarioModule} from "../usuario/usuario.module";
import {LancamentoNotasDetalhesPage} from "./pages/lancamento-notas-detalhes/lancamento-notas-detalhes.page";
import {UnidadeModule} from "../unidade/unidade.module";

@NgModule({
    declarations: [
        LancamentoNotasPage,
        LancamentoNotasDetalhesPage
    ],
    entryComponents: [
        LancamentoNotasPage,
        LancamentoNotasDetalhesPage
    ],
    exports: [],
    imports: [
        EscolaModule,
        UsuarioModule,
        UnidadeModule,
        IonicModule.forRoot(LancamentoNotasPage),
        SharedModule
    ],
    bootstrap: [IonicApp],
    providers: [
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        }
    ]
})

export class LancamentoNotasModule {

}
