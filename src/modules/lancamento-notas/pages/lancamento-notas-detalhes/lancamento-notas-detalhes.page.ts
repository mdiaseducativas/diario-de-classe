import {NavController, NavParams} from 'ionic-angular';
import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';

import {Usuario} from "../../../usuario/shared/models/usuario.model";
import {Page} from "../../../../shared/core/base.page";
import {EscolaRectComponent} from "../../../escola/components/rect/escola-rect.component";
import {LancamentoNotasDetalhesEscolaMediator} from "./shared/mediators/lancamento-notas-detalhes-escola.mediator";
import IFacade = puremvc.IFacade;
import {AppFacade} from "../../../../shared/core/application-facade";
import {UsuarioUnidadesComponent} from "../../../usuario/components/unidades/usuario-unidades.component";

@Component({
    selector: 'lancamento-notas-detalhes-page',
    templateUrl: 'lancamento-notas-detalhes.page.html',
    providers: []
})

export class LancamentoNotasDetalhesPage extends Page implements OnInit, AfterViewInit {

    @ViewChild(EscolaRectComponent)
    public escolaRectComponent: EscolaRectComponent;

    @ViewChild(UsuarioUnidadesComponent)
    public usuarioUnidadesComponent: UsuarioUnidadesComponent;

    protected usuario: Usuario;
    protected navParams: NavParams;
    protected navController: NavController;

    public lancamentoNotasDetalhesEscolaMediator: LancamentoNotasDetalhesEscolaMediator;

    constructor(navParams: NavParams, navController: NavController) {
        super();
        this.navParams = navParams;
        this.navController = navController;
        this.usuario = navParams.data.usuario || new Usuario();
    }

    public ngOnInit(): void {

        this.lancamentoNotasDetalhesEscolaMediator = new LancamentoNotasDetalhesEscolaMediator(this);

    }

    public ngAfterViewInit(): void {

        this.lancamentoNotasDetalhesEscolaMediator.readEscolaStorage();

    }

    public ngOnDestroy(): void {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);

        this.lancamentoNotasDetalhesEscolaMediator.removeAllSubscribers();

        if (facade.hasMediator(LancamentoNotasDetalhesEscolaMediator.NAME)) {
            facade.removeMediator(LancamentoNotasDetalhesEscolaMediator.NAME);
        }

    }

    protected goBack(): void {

        this.navController.pop();

    }

}
