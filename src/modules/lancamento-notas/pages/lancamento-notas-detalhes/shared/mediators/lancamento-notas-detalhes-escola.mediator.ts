import {Response} from '@angular/http';

import {BaseMediator} from "../../../../../../shared/core/base.mediator";
import {LancamentoNotasDetalhesPage} from "../../lancamento-notas-detalhes.page";
import {EscolaRectComponent} from "../../../../../escola/components/rect/escola-rect.component";
import {EscolaStorageNotifications} from "../../../../../escola/shared/notifications/escola-storage.notifications";
import {EscolaFiltro} from "../../../../../escola/shared/models/escola-filtro.model";
import {Escola} from "../../../../../escola/shared/models/escola.model";
import {Turma} from "../../../../../turma/shared/models/turma.model";
import {AnoLetivo} from "../../../../../ano-letivo/shared/models/ano-letivo.model";
import {UsuarioUnidadesComponent} from "../../../../../usuario/components/unidades/usuario-unidades.component";

export class LancamentoNotasDetalhesEscolaMediator extends BaseMediator {

    public static NAME: string = 'LancamentoNotasDetalhesEscolaMediator.NAME';

    private lancamentoNotasDetalhesPage: LancamentoNotasDetalhesPage;
    private usuarioUnidadesComponent: UsuarioUnidadesComponent;
    private escolaRectComponent: EscolaRectComponent;

    constructor(lancamentoNotasDetalhesPage: LancamentoNotasDetalhesPage) {

        super(LancamentoNotasDetalhesEscolaMediator.NAME);

        this.lancamentoNotasDetalhesPage = lancamentoNotasDetalhesPage;
        this.escolaRectComponent = lancamentoNotasDetalhesPage.escolaRectComponent;
        this.usuarioUnidadesComponent = lancamentoNotasDetalhesPage.usuarioUnidadesComponent;

    }

    public readEscolaStorage() {

        this.sendNotification(EscolaStorageNotifications.READ_ESCOLA_FILTRO);

    }

    /** @override */
    public registerListeners(): void {

        this.addListener(EscolaStorageNotifications.SUCCESS_READ_ESCOLA_FILTRO, this.onSuccessReadEscolaFiltro);
        this.addListener(EscolaStorageNotifications.FAILURE_SAVE_ESCOLA_FILTRO, this.onFailureReadEscolaFiltro);

    }

    private onSuccessReadEscolaFiltro(escolaFiltro: EscolaFiltro): void {

        let escola: Escola = escolaFiltro.escola;
        let turma: Turma = escolaFiltro.turma;
        let anoLetivo: AnoLetivo = escolaFiltro.anoLetivo;

        this.escolaRectComponent.setEscola(escola);
        this.escolaRectComponent.setTurma(turma);
        this.escolaRectComponent.setAnoLetivo(anoLetivo);

    }

    private onFailureReadEscolaFiltro(response: Response): void {

    }

}
