import {ModalController, NavController} from "ionic-angular";
import {Response} from '@angular/http';

import {BaseMediator} from "../../../../../../shared/core/base.mediator";
import {LancamentoNotasPage} from "../../lancamento-notas.page";
import {UsuarioGridComponent} from "../../../../../usuario/components/grid/usuario-grid.component";
import {UsuarioListNotasComponent} from "../../../../../usuario/components/list-notas/usuario-list-notas.component";
import {UsuarioFiltro} from "../../../../../usuario/shared/models/usuario-filtro.model";
import {Usuario} from "../../../../../usuario/shared/models/usuario.model";
import {UsuarioNotifications} from "../../../../../usuario/shared/notifications/usuario.notifications";
import {LancamentoNotasDetalhesPage} from "../../../lancamento-notas-detalhes/lancamento-notas-detalhes.page";

export class LancamentoNotasUsuarioMediator extends BaseMediator {

    public static NAME: string = 'LancamentoNotasUsuarioMediator.NAME';

    private lancamentoNotasPage: LancamentoNotasPage;
    private usuarioGridComponent: UsuarioGridComponent;
    private usuarioListNotasComponent: UsuarioListNotasComponent;
    private modalController: ModalController;
    private usuarioFiltro: UsuarioFiltro;
    private navController: NavController;

    constructor(lancamentoNotasPage: LancamentoNotasPage) {

        super(LancamentoNotasUsuarioMediator.NAME);

        this.lancamentoNotasPage = lancamentoNotasPage;
        this.modalController = lancamentoNotasPage.modalController;
        this.navController = lancamentoNotasPage.navController;
        this.usuarioGridComponent = lancamentoNotasPage.usuarioGridComponent;
        this.usuarioListNotasComponent = lancamentoNotasPage.usuarioListNotasComponent;
        this.usuarioFiltro = new UsuarioFiltro();

        this.subscribe(this.usuarioGridComponent.onSelectUsuario, this.onSelectUsuario);

    }

    private onSelectUsuario(usuario: Usuario): void {

        let usuarioFiltro: UsuarioFiltro = new UsuarioFiltro();

        usuarioFiltro.usuario = usuario;

        this.sendNotification(UsuarioNotifications.READ_LANCAMENTO_NOTAS_USUARIO, usuarioFiltro);

    }

    public readUsuarios(): void {

        this.usuarioGridComponent.setLoading(true);

        this.sendNotification(UsuarioNotifications.READ_USUARIOS, this.usuarioFiltro);

    }

    /** @override */
    public registerListeners(): void {

        this.addListener(UsuarioNotifications.SUCCESS_READ_USUARIOS, this.onSuccessReadUsuarios);
        this.addListener(UsuarioNotifications.FAILURE_READ_USUARIOS, this.onFailureReadUsuarios);
        this.addListener(UsuarioNotifications.SUCCESS_READ_LANCAMENTO_NOTAS_USUARIO, this.onSuccessReadLancamentoNotasUsuario);
        this.addListener(UsuarioNotifications.FAILURE_READ_LANCAMENTO_NOTAS_USUARIO, this.onFailureLancamentoNotasUsuario);

    }

    private onSuccessReadLancamentoNotasUsuario(usuario: Usuario): void {

        this.navController.push(LancamentoNotasDetalhesPage, {usuario});

    }

    private onModalDidDimiss(): void {

        console.log('onModalDidDimiss');

    }

    private onFailureLancamentoNotasUsuario(response: Response): void {

    }

    private onSuccessReadUsuarios(usuarios: Usuario[]): void {

        this.usuarioGridComponent.setLoading(false);
        this.usuarioGridComponent.setUsuarios(usuarios);

    }

    private onFailureReadUsuarios(): void {

        this.usuarioGridComponent.setLoading(false);
        this.usuarioGridComponent.setUsuarios([]);

    }

}
