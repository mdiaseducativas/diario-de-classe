import {ModalController} from "ionic-angular";

import {BaseMediator} from "../../../../../../shared/core/base.mediator";
import {FrequenciaPage} from "../../frequencia.page";
import {EscolaFiltro} from "../../../../../escola/shared/models/escola-filtro.model";
import {EscolaRectContainerComponent} from "../../../../../escola/components/rect-container/escola-rect-container.component";
import {LancamentoNotasPage} from "../../lancamento-notas.page";

export class LancamentoNotasEscolaMediator extends BaseMediator {

    public static NAME: string = 'LancamentoNotasEscolaMediator.NAME';

    private lancamentoNotasPage: LancamentoNotasPage;
    private modalController: ModalController;
    private escolaRectContainerComponent: EscolaRectContainerComponent;

    constructor(lancamentoNotasPage: LancamentoNotasPage) {

        super(LancamentoNotasEscolaMediator.NAME);

        this.lancamentoNotasPage = lancamentoNotasPage;
        this.escolaRectContainerComponent = lancamentoNotasPage.escolaRectContainerComponent;
        this.modalController = lancamentoNotasPage.modalController;

        this.subscribe(this.escolaRectContainerComponent.onChangeEscola, this.onChangeEscola);

    }

    private onChangeEscola(escolaFiltro: EscolaFiltro): void {

        if(!escolaFiltro) {
            return;
        }

        console.log('onChangeEscola');
        this.lancamentoNotasPage.escolaFiltro = escolaFiltro;
        // this.lancamentoNotasPage.horariosRectContainerComponent.reset();
        // this.lancamentoNotasPage.frequenciaUsuariosMediator.readUsuarios();

    }

}
