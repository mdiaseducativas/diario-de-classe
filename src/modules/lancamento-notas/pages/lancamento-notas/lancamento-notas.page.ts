import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ModalController, NavController} from "ionic-angular";

import {Page} from "../../../../shared/core/base.page";
import {FrequenciaHorariosMediator} from "./shared/mediators/frequencia-horarios.mediator";
import {EscolaFiltro} from "../../../escola/shared/models/escola-filtro.model";
import {FrequenciaUsuariosMediator} from "./shared/mediators/frequencia-usuarios.mediator";
import {EscolaRectContainerComponent} from "../../../escola/components/rect-container/escola-rect-container.component";
import {FrequenciaEscolaMediator} from "./shared/mediators/frequencia-escola.mediator";
import {LancamentoNotasEscolaMediator} from "./shared/mediators/lancamento-notas-escola.mediator";
import {UsuarioListNotasComponent} from "../../../usuario/components/list-notas/usuario-list-notas.component";
import {UsuarioGridComponent} from "../../../usuario/components/grid/usuario-grid.component";
import {LancamentoNotasUsuarioMediator} from "./shared/mediators/lancamento-notas-usuario.mediator";
import {RectUnidadesComponent} from "../../../unidade/components/rect-unidades/rect-unidades.component";

@Component({
    selector: 'lancamento-notas',
    templateUrl: 'lancamento-notas.page.html',
    providers: []
})

export class LancamentoNotasPage extends Page implements OnInit, AfterViewInit {

    @ViewChild(EscolaRectContainerComponent)
    public escolaRectContainerComponent: EscolaRectContainerComponent;

    @ViewChild(UsuarioGridComponent)
    public usuarioGridComponent: UsuarioGridComponent;

    @ViewChild(UsuarioListNotasComponent)
    public usuarioListNotasComponent: UsuarioListNotasComponent;

    @ViewChild(RectUnidadesComponent)
    public rectUnidadesComponent: RectUnidadesComponent;

    public escolaFiltro: EscolaFiltro;
    public navController: NavController;
    public modalController: ModalController;
    public lancamentoNotasEscolaMediator: LancamentoNotasEscolaMediator;
    public lancamentoNotasUsuarioMediator: LancamentoNotasUsuarioMediator;

    constructor(navController: NavController, modalController: ModalController) {

        super();

        this.navController = navController;
        this.modalController = modalController;

    }

    public ngOnInit(): void {

        this.lancamentoNotasEscolaMediator = new LancamentoNotasEscolaMediator(this);
        this.lancamentoNotasUsuarioMediator = new LancamentoNotasUsuarioMediator(this);

        this.addMediator(this.lancamentoNotasEscolaMediator);
        this.addMediator(this.lancamentoNotasUsuarioMediator);

    }

    public ngAfterViewInit(): void {

        this.lancamentoNotasUsuarioMediator.readUsuarios();

    }

}
