import {Component, EventEmitter, Output} from '@angular/core';

import {Usuario} from "../../../usuario/shared/models/usuario.model";

@Component({
    selector: 'login-form-component',
    templateUrl: 'login-form.component.html',
    providers: []
})

export class LoginFormComponent {

    protected loading: boolean;
    protected usuario: Usuario;

    @Output() onSubmit: EventEmitter<Usuario>;

    constructor() {

        this.loading = false;
        this.usuario = new Usuario();
        this.onSubmit = new EventEmitter<Usuario>();

    }

    protected submit(form: any): void {

        if (form.valid) {
            this.onSubmit.emit(this.usuario);
        }

    }

    public setLoading(loading: boolean) {

        this.loading = loading;

    }

    public getUsuario(): Usuario {

        return this.usuario;

    }

    public setUsuario(usuario: Usuario): void {

        this.usuario = usuario;

    }

}
