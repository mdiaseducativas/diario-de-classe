import {NavController} from "ionic-angular";

import {LoginPage} from "../../login.page";
import {LoginNotifications} from "../../../shared/notifications/login.notification";
import {StatusBarNotifications} from "../../../../../common/status-bar/shared/notifications/status-bar.notifications";
import {BaseMediator} from "../../../../../shared/core/base.mediator";
import {LoginFormComponent} from "../../../components/login-form/login-form.component";
import {Usuario} from "../../../../usuario/shared/models/usuario.model";
import {IonicEssentials} from "../../../../../shared/model/ionic-essentials/ionic-essentials.model";
import {Result} from "../../../../../shared/model/result/result.model";
import {UsuarioStorageNotifications} from "../../../../usuario/shared/notifications/usuario-storage.notifications";
import {ConfigNotifications} from "../../../../../common/config/shared/notifications/config.notifications";
import {EscolaPage} from "../../../../escola/page/escola.page";

export class LoginMediator extends BaseMediator {

    public static NAME: string = 'LoginMediator.NAME';

    private loginFormComponent: LoginFormComponent;
    private navController: NavController;

    constructor(loginPage: LoginPage) {

        super(LoginMediator.NAME);

        this.navController = loginPage.navController;
        this.loginFormComponent = loginPage.loginFormComponent;

        this.getUserStorage();

        this.subscribe(this.loginFormComponent.onSubmit, this.login);

        this.sendNotification(StatusBarNotifications.CHANGE_TO_WHITE);
        this.sendNotification(ConfigNotifications.READ_CONFIG);

    }

    private getUserStorage(): void {

        this.loginFormComponent.setLoading(true);

        this.sendNotification(UsuarioStorageNotifications.READ_USUARIO);

    }

    private login(): void {

        this.loginFormComponent.setLoading(true);

        let usuario: Usuario = this.loginFormComponent.getUsuario();

        this.sendNotification(LoginNotifications.LOGIN, usuario);

    }

    private openEscolas(): void {

        this.navController.setRoot(EscolaPage);

    }

    /** @override */
    public registerListeners(): void {

        this.addListener(LoginNotifications.SUCCESS_LOGIN, this.onSuccessLogin);
        this.addListener(LoginNotifications.FAILURE_LOGIN, this.onFailureLogin);
        this.addListener(UsuarioStorageNotifications.SUCCESS_READ_USUARIO, this.onSuccessReadUsuario);
        this.addListener(UsuarioStorageNotifications.FAILURE_READ_USUARIO, this.onFailureReadUser);

    }

    private onSuccessReadUsuario(usuario: Usuario): void {

        if (usuario && usuario.login && usuario.cpf && usuario.senha) {
            this.loginFormComponent.setUsuario(usuario);
            return this.login();
        }

        this.loginFormComponent.setLoading(false);

    }

    private onFailureReadUser(): void {

        this.loginFormComponent.setLoading(false);

    }

    private onSuccessLogin(usuario: Usuario): void {

        // TODO: register push

        this.loginFormComponent.setLoading(false);

        this.openEscolas();

    }

    private onFailureLogin(result: Result): void {

        this.loginFormComponent.setLoading(false);

        this.sendNotification(LoginNotifications.LOGOUT);

        let title: string = 'Falha no login!';
        let subtitle: string = 'E-mail, CPF ou senha inválidos';

        IonicEssentials.alert.basic(result.response, title, subtitle);

    }

}
