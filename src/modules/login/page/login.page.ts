import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';

import {Page} from "../../../shared/core/base.page";
import {UsuarioService} from "../../usuario/shared/services/usuario.service";
import {LoginFormComponent} from "../components/login-form/login-form.component";
import {LoginMediator} from "./shared/mediators/login.mediator";
import {AlertController, NavController} from "ionic-angular";

@Component({
    selector: 'login-page',
    templateUrl: 'login.page.html',
    providers: [UsuarioService]
})

export class LoginPage extends Page implements OnInit, OnDestroy {

    @ViewChild(LoginFormComponent) public loginFormComponent: LoginFormComponent;

    public loginMediator: LoginMediator;
    public navController: NavController;

    constructor(navController: NavController) {

        super();

        this.navController = navController;

    }

    public ngOnInit(): void {

        this.loginMediator = new LoginMediator(this);

        this.addMediator(this.loginMediator);

    }

}
