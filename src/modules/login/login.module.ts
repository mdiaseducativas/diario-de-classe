import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {SharedModule} from "../../components/shared.module";
import {ConfigService} from "../../common/config/shared/services/config.service";
import {Authentication} from "../../common/authentication/shared/services/authentication.service";
import {LoginPage} from "./page/login.page";
import {LoginFormComponent} from "./components/login-form/login-form.component";
import {AppFacade} from "../../shared/core/application-facade";
import {LoginProxy} from "./shared/models/login.proxy";
import IFacade = puremvc.IFacade;
import {LoginNotifications} from "./shared/notifications/login.notification";
import {LoginCommand} from "./shared/controllers/commands/login.command";
import {LogoutCommand} from "./shared/controllers/commands/logout.command";
import {LoginEscolaCommand} from "./shared/controllers/commands/login-escola.command";

@NgModule({
    declarations: [
        LoginPage,
        LoginFormComponent
    ],
    imports: [
        IonicModule.forRoot(LoginPage),
        SharedModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        LoginPage,
        LoginFormComponent
    ],
    providers: [
        Authentication,
        ConfigService,
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        }
    ]
})

export class LoginModule {

    constructor(authentication: Authentication) {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
        let loginProxy: LoginProxy = new LoginProxy();
        loginProxy.service = authentication;

        /** Register Proxies */
        facade.registerProxy(loginProxy);

        /** Register Commands */
        facade.registerCommand(LoginNotifications.LOGIN, LoginCommand);
        facade.registerCommand(LoginNotifications.LOGOUT, LogoutCommand);
        facade.registerCommand(LoginNotifications.LOGIN_SCHOOL, LoginEscolaCommand);

    }

}
