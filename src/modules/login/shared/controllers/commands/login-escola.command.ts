import SimpleCommand = puremvc.SimpleCommand;
import ICommand = puremvc.ICommand;
import IFacade = puremvc.IFacade;
import INotification = puremvc.INotification;
import {Response} from '@angular/http';

import {AppFacade} from "../../../../../shared/core/application-facade";
import {LoginProxy} from "../../models/login.proxy";
import {Observable} from "rxjs/Observable";
import {LoginNotifications} from "../../notifications/login.notification";
import {Result} from "../../../../../shared/model/result/result.model";
import {Authentication} from "../../../../../common/authentication/shared/services/authentication.service";
import {EscolaFiltro} from "../../../../escola/shared/models/escola-filtro.model";

export class LoginEscolaCommand extends SimpleCommand implements ICommand {

    /** @override */
    public execute(notification: INotification): void {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
        let proxy: LoginProxy = facade.retrieveProxy(LoginProxy.NAME) as LoginProxy;

        let escolaFiltro: EscolaFiltro = notification.getBody();
        let observer: Observable<Object> = proxy.loginSchool(escolaFiltro);
        observer.subscribe(
            (data: Object) => this.onSuccessLogin(data),
            (response: Response) => this.onFailureLogin(response)
        );

    }

    private onSuccessLogin(data: Object) {

        let token: string = data['token'];

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
        let proxy: LoginProxy = facade.retrieveProxy(LoginProxy.NAME) as LoginProxy;
        let authentication: Authentication = proxy.service;

        authentication.setSchoolToken(token);

        this.sendNotification(LoginNotifications.SUCCESS_LOGIN_SCHOOL);

    }

    private onFailureLogin(response: Response) {

        let result: Result = new Result();
        result.response = response;

        this.sendNotification(LoginNotifications.FAILURE_LOGIN_SCHOOL, result);

    }

}
