import SimpleCommand = puremvc.SimpleCommand;
import ICommand = puremvc.ICommand;
import IFacade = puremvc.IFacade;
import INotification = puremvc.INotification;

import {AppFacade} from "../../../../../shared/core/application-facade";
import {LoginProxy} from "../../models/login.proxy";
import {LoginNotifications} from "../../notifications/login.notification";
import {UsuarioStorageNotifications} from "../../../../usuario/shared/notifications/usuario-storage.notifications";
import {EscolaStorageNotifications} from "../../../../escola/shared/notifications/escola-storage.notifications";

export class LogoutCommand extends SimpleCommand implements ICommand {

    /** @override */
    public execute(notification: INotification): void {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
        let proxy: LoginProxy = facade.retrieveProxy(LoginProxy.NAME) as LoginProxy;

        proxy.logout();

        this.sendNotification(UsuarioStorageNotifications.DELETE_USUARIO);
        this.sendNotification(EscolaStorageNotifications.DELETE_ESCOLA_FILTRO);
        this.sendNotification(LoginNotifications.SUCCESS_LOGOUT);

    }

}
