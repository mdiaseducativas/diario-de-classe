import SimpleCommand = puremvc.SimpleCommand;
import ICommand = puremvc.ICommand;
import IFacade = puremvc.IFacade;
import INotification = puremvc.INotification;
import {Response} from '@angular/http';

import {AppFacade} from "../../../../../shared/core/application-facade";
import {LoginProxy} from "../../models/login.proxy";
import {Observable} from "rxjs/Observable";
import {Usuario} from "../../../../usuario/shared/models/usuario.model";
import {LoginNotifications} from "../../notifications/login.notification";
import {UsuarioFactory} from "../../../../usuario/shared/models/usuario.factory";
import {UsuarioStorageNotifications} from "../../../../usuario/shared/notifications/usuario-storage.notifications";
import {Result} from "../../../../../shared/model/result/result.model";
import {AnoLetivoFactory} from "../../../../ano-letivo/shared/models/ano-letivo.factory";
import {AnoLetivo} from "../../../../ano-letivo/shared/models/ano-letivo.model";
import {AnoLetivoStorageNotifications} from "../../../../ano-letivo/shared/notifications/ano-letivo-storage.notifications";
import {Authentication} from "../../../../../common/authentication/shared/services/authentication.service";

export class LoginCommand extends SimpleCommand implements ICommand {

    private usuario: Usuario;

    constructor() {
        super();
        this.usuario = new Usuario();
    }

    /** @override */
    public execute(notification: INotification): void {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
        let proxy: LoginProxy = facade.retrieveProxy(LoginProxy.NAME) as LoginProxy;

        let usuario: Usuario = notification.getBody();
        this.usuario = usuario;

        let observer: Observable<Object> = proxy.loginAuthentication(usuario);
        observer.subscribe(
            (data: Object) => this.onSuccessLogin(data),
            (response: Response) => this.onFailureLogin(response)
        );

    }

    private onSuccessLogin(data: Object) {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
        let proxy: LoginProxy = facade.retrieveProxy(LoginProxy.NAME) as LoginProxy;

        let token: string = data['token'];
        let authentication: Authentication = proxy.service;
        authentication.setAuthenticationToken(token);

        let usuarioFactory = new UsuarioFactory();
        let usuario: Usuario = usuarioFactory.create(data['usuario']);
        usuario.login = this.usuario.login;
        usuario.cpf = this.usuario.cpf;
        usuario.senha = this.usuario.senha;

        let anoLetivoFactory = new AnoLetivoFactory();
        let anosLetivos: AnoLetivo[] = anoLetivoFactory.create(data['anos-letivos']);

        this.sendNotification(UsuarioStorageNotifications.SAVE_USUARIO, usuario);
        this.sendNotification(AnoLetivoStorageNotifications.SAVE_ANOS_LETIVOS, anosLetivos);

        setTimeout(() => this.sendNotification(LoginNotifications.SUCCESS_LOGIN, data), 500);

    }

    private onFailureLogin(response: Response) {

        let result: Result = new Result();
        result.response = response;

        this.sendNotification(LoginNotifications.FAILURE_LOGIN, result);

    }

}
