export class LoginNotifications {

    public static LOGIN: string = "LoginNotifications.LOGIN";
    public static SUCCESS_LOGIN: string = "LoginNotifications.SUCCESS_LOGIN";
    public static FAILURE_LOGIN: string = "LoginNotifications.FAILURE_LOGIN";

    public static LOGIN_SCHOOL: string = "LoginNotifications.LOGIN_SCHOOL";
    public static SUCCESS_LOGIN_SCHOOL: string = "LoginNotifications.SUCCESS_LOGIN_SCHOOL";
    public static FAILURE_LOGIN_SCHOOL: string = "LoginNotifications.FAILURE_LOGIN_SCHOOL";

    public static LOGOUT: string = "LoginNotifications.LOGOUT";
    public static SUCCESS_LOGOUT: string = "LoginNotifications.SUCCESS_LOGOUT";
    public static FAILURE_LOGOUT: string = "LoginNotifications.FAILURE_LOGOUT";

    public static GET_IS_LOGGED: string = "LoginNotifications.GET_IS_LOGGED";
    public static RESULT_GET_IS_LOGGED: string = "LoginNotifications.RESULT_GET_IS_LOGGED";

}
