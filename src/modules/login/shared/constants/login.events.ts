export class LoginEvents {

    public static GET_TOKEN: string = "LoginEvents.GET_TOKEN";
    public static LOGOUT: string = "LoginEvents.LOGOUT";

}
