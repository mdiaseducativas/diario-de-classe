import IProxy = puremvc.IProxy;
import Proxy = puremvc.Proxy;
import {Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import {Authentication} from "../../../../common/authentication/shared/services/authentication.service";
import {EscolaFiltro} from "../../../escola/shared/models/escola-filtro.model";
import {Usuario} from "../../../usuario/shared/models/usuario.model";

export class LoginProxy extends Proxy implements IProxy {

    public static NAME: string = "LoginProxy.NAME";

    public service: Authentication;

    constructor(data?: any) {

        super(LoginProxy.NAME, data);

    }

    public loginAuthentication(usuario: Usuario): Observable<Object> {

        let observer: Observable<Response> = this.service.loginAuthentication(usuario);
        return observer
            .map((res: Response) => res.json())
            .map((data: Object) => data['data']);

    }

    public loginSchool(escolaFiltro: EscolaFiltro): Observable<Object> {

        let observer: Observable<Response> = this.service.loginSchool(escolaFiltro);
        return observer
            .map((res: Response) => res.json())
            .map((data: Object) => data['data']);

    }

    public getIsLogged(): boolean {

        return this.service.isLogged();

    }

    public logout(): void {

        this.service.logout();

    }

    public getService(): Authentication {

        return this.service;

    }

}
