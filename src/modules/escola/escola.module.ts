import IFacade = puremvc.IFacade;
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {SharedModule} from "../../components/shared.module";
import {AppFacade} from "../../shared/core/application-facade";
import {EscolaStorageNotifications} from "./shared/notifications/escola-storage.notifications";
import {EscolaGridComponent} from "./components/grid/escola-grid.component";
import {EscolaPage} from "./page/escola.page";
import {AnoLetivoModule} from "../ano-letivo/ano-letivo.module";
import {EscolaNotifications} from "./shared/notifications/escola.notifications";
import {ReadEscolasCommand} from "./shared/controllers/commands/read-escolas.command";
import {EscolaService} from "./shared/services/escola.service";
import {EscolaProxy} from "./shared/models/escola.proxy";
import {TurmaModule} from "../turma/turma.module";
import {EscolaRectComponent} from "./components/rect/escola-rect.component";
import {ReadEscolaFiltroStorageCommand} from "./shared/controllers/commands/storage/read-escola-filtro-storage.command";
import {SaveEscolaFiltroStorageCommand} from "./shared/controllers/commands/storage/save-escola-filtro-storage.command";
import {DeleteEscolaFiltroStorageCommand} from "./shared/controllers/commands/storage/delete-escola-filtro-storage.command";
import {EscolaFiltroModalComponent} from "./components/filtro-modal/escola-filtro-modal.component";
import {EscolaRectContainerComponent} from "./components/rect-container/escola-rect-container.component";

@NgModule({
    declarations: [
        EscolaPage,
        EscolaGridComponent,
        EscolaRectComponent,
        EscolaFiltroModalComponent,
        EscolaRectContainerComponent
    ],
    entryComponents: [
        EscolaPage,
        EscolaGridComponent,
        EscolaRectComponent,
        EscolaFiltroModalComponent,
        EscolaRectContainerComponent
    ],
    exports: [
        EscolaPage,
        EscolaGridComponent,
        EscolaRectComponent,
        EscolaFiltroModalComponent,
        EscolaRectContainerComponent
    ],
    imports: [
        IonicModule.forRoot(EscolaPage),
        AnoLetivoModule,
        TurmaModule,
        SharedModule
    ],
    bootstrap: [IonicApp],
    providers: [
        EscolaService,
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        }
    ]
})

export class EscolaModule {

    constructor(escolaService: EscolaService) {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
        let escolaProxy: EscolaProxy = new EscolaProxy();
        escolaProxy.service = escolaService;

        /** Register Proxies */
        facade.registerProxy(escolaProxy);

        /** Register Commands */
        facade.registerCommand(EscolaNotifications.READ_ESCOLAS, ReadEscolasCommand);
        facade.registerCommand(EscolaStorageNotifications.SAVE_ESCOLA_FILTRO, SaveEscolaFiltroStorageCommand);
        facade.registerCommand(EscolaStorageNotifications.READ_ESCOLA_FILTRO, ReadEscolaFiltroStorageCommand);
        facade.registerCommand(EscolaStorageNotifications.DELETE_ESCOLA_FILTRO, DeleteEscolaFiltroStorageCommand);

    }

}
