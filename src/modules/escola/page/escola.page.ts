import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';

import {Page} from "../../../shared/core/base.page";
import {LoginFormComponent} from "../components/login-form/login-form.component";
import {LoginMediator} from "./shared/mediators/login.mediator";
import {NavController} from "ionic-angular";
import {EscolaMediator} from "./shared/mediators/escola.mediator";
import {AnoLetivoMediator} from "./shared/mediators/ano-letivo.mediator";
import {SelectAnoLetivoComponent} from "../../ano-letivo/components/select-ano-letivo/select-ano-letivo.component";
import {EscolaGridComponent} from "../components/grid/escola-grid.component";
import {TurmaGridComponent} from "../../turma/components/turma-grid/turma-grid.component";
import {EscolaFiltro} from "../shared/models/escola-filtro.model";
import {TurmaMediator} from "./shared/mediators/turma.mediator";

@Component({
    selector: 'escola-page',
    templateUrl: 'escola.page.html',
    providers: []
})

export class EscolaPage extends Page implements OnInit {

    @Input() modal: boolean;
    @Input() textSaveButton: string;

    @Output() onChangeEscolaFiltro: EventEmitter<EscolaFiltro>;

    @ViewChild(SelectAnoLetivoComponent) selectAnoLetivoComponent: SelectAnoLetivoComponent;
    @ViewChild(EscolaGridComponent) escolaGridComponent: EscolaGridComponent;
    @ViewChild(TurmaGridComponent) turmaGridComponent: TurmaGridComponent;

    public escolaFiltro: EscolaFiltro;
    public navController: NavController;
    public escolaMediator: EscolaMediator;
    public anoLetivoMediator: AnoLetivoMediator;
    public turmaMediator: TurmaMediator;

    protected loading: boolean;

    constructor(navController: NavController) {

        super();

        this.modal = false;
        this.loading = false;
        this.navController = navController;
        this.escolaFiltro = new EscolaFiltro();
        this.onChangeEscolaFiltro = new EventEmitter<EscolaFiltro>();

        this.textSaveButton = 'Avançar';

    }

    public ngOnInit(): void {

        this.escolaMediator = new EscolaMediator(this);
        this.anoLetivoMediator = new AnoLetivoMediator(this);
        this.turmaMediator = new TurmaMediator(this);

        this.addMediator(this.escolaMediator);
        this.addMediator(this.anoLetivoMediator);
        this.addMediator(this.turmaMediator);

    }

    public setLoading(loading: boolean): void {

        this.loading = loading;

    }

}
