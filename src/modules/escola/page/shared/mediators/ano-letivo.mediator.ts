import {BaseMediator} from "../../../../../shared/core/base.mediator";
import {EscolaPage} from "../../escola.page";
import {SelectAnoLetivoComponent} from "../../../../ano-letivo/components/select-ano-letivo/select-ano-letivo.component";
import {AnoLetivoStorageNotifications} from "../../../../ano-letivo/shared/notifications/ano-letivo-storage.notifications";
import {AnoLetivo} from "../../../../ano-letivo/shared/models/ano-letivo.model";
import {EscolaFiltro} from "../../../shared/models/escola-filtro.model";
import {EscolaStorageNotifications} from "../../../shared/notifications/escola-storage.notifications";

export class AnoLetivoMediator extends BaseMediator {

    public static NAME: string = 'AnoLetivoMediator.NAME';

    protected escolaPage: EscolaPage;
    protected selectAnoLetivoComponent: SelectAnoLetivoComponent;

    constructor(escolaPage: EscolaPage) {

        super(AnoLetivoMediator.NAME);

        this.escolaPage = escolaPage;
        this.selectAnoLetivoComponent = escolaPage.selectAnoLetivoComponent;

        this.subscribe(this.selectAnoLetivoComponent.onSelectAnoLetivo, this.onSelectAnoLetivo);

    }

    private readAnosLetivos(): void {

        this.sendNotification(AnoLetivoStorageNotifications.READ_ANOS_LETIVOS);

    }

    private onSelectAnoLetivo(anoLetivo: AnoLetivo): void {

        let escolaFiltro: EscolaFiltro = this.escolaPage.escolaFiltro;
        escolaFiltro.anoLetivo = anoLetivo;
        escolaFiltro.escola = null;
        escolaFiltro.turma = null;
        escolaFiltro.disciplina = null;

        this.escolaPage.escolaMediator.readEscolas();

    }

    /** @override */
    public registerListeners(): void {

        this.addListener(EscolaStorageNotifications.SUCCESS_READ_ESCOLA_FILTRO, this.onSuccessReadEscolaFiltro);
        this.addListener(EscolaStorageNotifications.FAILURE_READ_ESCOLA_FILTRO, this.readAnosLetivos);

        this.addListener(AnoLetivoStorageNotifications.SUCCESS_READ_ANOS_LETIVOS, this.onSuccessReadAnosLetivos);
        this.addListener(AnoLetivoStorageNotifications.FAILURE_READ_ANOS_LETIVOS, this.onFailureReadAnosLetivos);

    }

    private onSuccessReadEscolaFiltro(): void {

        this.readAnosLetivos();

    }

    private onSuccessReadAnosLetivos(anosLetivos: AnoLetivo[]): void {

        if (!anosLetivos || !anosLetivos.length) {
            return;
        }

        let escolaFiltro: EscolaFiltro = this.escolaPage.escolaFiltro;
        let anoLetivo: AnoLetivo = escolaFiltro.anoLetivo || anosLetivos[0];

        this.selectAnoLetivoComponent.setAnoLetivo(anoLetivo);
        this.selectAnoLetivoComponent.setAnosLetivos(anosLetivos);

        if (!escolaFiltro.isValid()) {
            this.onSelectAnoLetivo(anoLetivo);
            return;
        }

        this.escolaPage.escolaMediator.readEscolas();

    }

    private onFailureReadAnosLetivos(): void {

        console.error('onFailureReadAnosLetivos');

    }


}
