import {BaseMediator} from "../../../../../shared/core/base.mediator";
import {EscolaPage} from "../../escola.page";
import {TurmaGridComponent} from "../../../../turma/components/turma-grid/turma-grid.component";
import {Turma} from "../../../../turma/shared/models/turma.model";
import {EscolaFiltro} from "../../../shared/models/escola-filtro.model";
import {TurmaNotifications} from "../../../../turma/shared/notifications/turma.notifications";

export class TurmaMediator extends BaseMediator {

    public static NAME: string = 'TurmaMediator.NAME';

    protected escolaPage: EscolaPage;
    protected turmaGridComponent: TurmaGridComponent;

    constructor(escolaPage: EscolaPage) {

        super(TurmaMediator.NAME);

        this.escolaPage = escolaPage;
        this.turmaGridComponent = escolaPage.turmaGridComponent;

        this.subscribe(this.turmaGridComponent.onSelectTurma, this.onSelectTurma);

    }

    /** @override */
    public registerListeners(): void {

        this.addListener(TurmaNotifications.SUCCESS_READ_TURMAS, this.onSuccessReadTurmas);

    }

    private onSelectTurma(turma: Turma): void {

        let escolaFiltro: EscolaFiltro = this.escolaPage.escolaFiltro;
        escolaFiltro.turma = turma;
        escolaFiltro.disciplina = turma.disciplina;

    }

    private onSuccessReadTurmas(turmas: Turma[]): void {

        this.turmaGridComponent.setTurmas(turmas);

        let turma: Turma = this.escolaPage.escolaFiltro.turma;
        if (turma) {
            this.turmaGridComponent.setTurma(turma);
        }

    }

}
