import {BaseMediator} from "../../../../../shared/core/base.mediator";
import {EscolaPage} from "../../escola.page";
import {EscolaGridComponent} from "../../../components/grid/escola-grid.component";
import {Escola} from "../../../shared/models/escola.model";
import {EscolaNotifications} from "../../../shared/notifications/escola.notifications";
import {EscolaFiltro} from "../../../shared/models/escola-filtro.model";
import {TurmaNotifications} from "../../../../turma/shared/notifications/turma.notifications";
import {LoginNotifications} from "../../../../login/shared/notifications/login.notification";
import {HomePage} from "../../../../home/page/home.page";
import {EscolaStorageNotifications} from "../../../shared/notifications/escola-storage.notifications";
import {Result} from "../../../../../shared/model/result/result.model";
import {Turma} from "../../../../turma/shared/models/turma.model";

export class EscolaMediator extends BaseMediator {

    public static NAME: string = 'EscolaMediator.NAME';

    protected escolaPage: EscolaPage;
    protected escolaGridComponent: EscolaGridComponent;

    constructor(escolaPage: EscolaPage) {

        super(EscolaMediator.NAME);

        this.escolaPage = escolaPage;
        this.escolaGridComponent = escolaPage.escolaGridComponent;

        this.subscribe(this.escolaGridComponent.onSelectEscola, this.onSelectEscola);

        this.readEscolaFiltro();

    }

    private readEscolaFiltro(): void {

        this.sendNotification(EscolaStorageNotifications.READ_ESCOLA_FILTRO);

    }

    public loginEscola(): void {

        this.escolaPage.setLoading(true);

        this.sendNotification(LoginNotifications.LOGIN_SCHOOL, this.escolaPage.escolaFiltro);

    }

    public readEscolas(): void {

        let escolaFiltro: EscolaFiltro = this.escolaPage.escolaFiltro;

        this.escolaPage.setLoading(true);

        this.sendNotification(EscolaNotifications.READ_ESCOLAS, escolaFiltro);

    }

    /** @override */
    public registerListeners(): void {

        this.addListener(EscolaNotifications.SUCCESS_READ_ESCOLAS, this.onSuccessReadEscolas);
        this.addListener(EscolaNotifications.FAILURE_READ_ESCOLAS, this.onFailure);

        this.addListener(LoginNotifications.SUCCESS_LOGIN_SCHOOL, this.onSuccessLoginEscola);
        this.addListener(LoginNotifications.FAILURE_LOGIN_SCHOOL, this.onFailure);

        this.addListener(EscolaStorageNotifications.SUCCESS_SAVE_ESCOLA_FILTRO, this.onSuccessSaveEscolaFiltro);
        this.addListener(EscolaStorageNotifications.FAILURE_SAVE_ESCOLA_FILTRO, this.onFailure);

        this.addListener(EscolaStorageNotifications.SUCCESS_READ_ESCOLA_FILTRO, this.onSuccessReadEscolaFiltro);
        this.addListener(EscolaStorageNotifications.FAILURE_SAVE_ESCOLA_FILTRO, this.onFailure);

    }

    private onSelectEscola(escola: Escola): void {

        let escolaFiltro = this.escolaPage.escolaFiltro;
        escolaFiltro.escola = escola;
        escolaFiltro.turma = null;
        escolaFiltro.disciplina = null;

        let turmas = escola.turmas || [];

        this.sendNotification(TurmaNotifications.SUCCESS_READ_TURMAS, turmas);

    }

    private onSuccessReadEscolaFiltro(escolaFiltro: EscolaFiltro): void {

        this.escolaPage.escolaFiltro = escolaFiltro;

        if (escolaFiltro.isValid() && !this.escolaPage.modal) {
            this.loginEscola();
        }

    }

    private onSuccessReadEscolas(escolas: Escola[]): void {

        this.escolaPage.setLoading(false);
        this.escolaGridComponent.setEscolas(escolas);

        let escola: Escola = this.escolaPage.escolaFiltro.escola;
        if (!escola) {
            return;
        }

        this.escolaGridComponent.setEscola(escola);

        let turmas: Turma[] = escola.turmas || [];

        this.sendNotification(TurmaNotifications.SUCCESS_READ_TURMAS, turmas);

    }

    private onSuccessLoginEscola(): void {

        this.sendNotification(EscolaStorageNotifications.SAVE_ESCOLA_FILTRO, this.escolaPage.escolaFiltro);

    }

    private onSuccessSaveEscolaFiltro(): void {

        this.escolaPage.setLoading(false);

        if (!this.escolaPage.modal) {
            this.escolaPage.ngOnDestroy();
            this.escolaPage.navController.setRoot(HomePage);
            return;
        }

        this.escolaPage.onChangeEscolaFiltro.emit(this.escolaPage.escolaFiltro);

    }

    private onFailure(result: Result): void {

        this.escolaPage.setLoading(false);

    }

}
