import {Injectable} from '@angular/core';
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Rx";

import {Usuario} from "../models/usuario.model";
import {Authentication} from "../../../../common/authentication/shared/services/authentication.service";
import {Service} from "../../../../shared/services/base.service";
import {ConfigService} from "../../../../common/config/shared/services/config.service";
import {EscolaFiltro} from "../models/escola-filtro.model";

@Injectable()
export class EscolaService extends Service {

    private http: Http;
    private config: ConfigService;
    private authentication: Authentication;

    constructor(http: Http, config: ConfigService, authentication: Authentication) {

        super();

        this.http = http;
        this.config = config;
        this.authentication = authentication;

    }

    public readEscolas(escolaFiltro: EscolaFiltro): Observable<Response> {

        // let anoLetivo: AnoLetivo = escolaFiltro.anoLetivo;
        // let url: string = this.config.getURL() + '/ano/'+ anoLetivo.id_ano_letivo +'/escolas';
        //
        // return this.http.get(url, {
        //     headers: this.getHeaders()
        // });

        return this.http.get('assets/data/escolas.retorno.json');

    }

}
