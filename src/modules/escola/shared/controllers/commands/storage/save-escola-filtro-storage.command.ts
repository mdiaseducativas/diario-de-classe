import SimpleCommand = puremvc.SimpleCommand;
import ICommand = puremvc.ICommand;
import INotification = puremvc.INotification;
import {Storage} from "@ionic/storage";

import {EscolaStorageNotifications} from "../../../notifications/escola-storage.notifications";
import {EscolaConstants} from "../../../constants/escola.constants";
import {IonicEssentials} from "../../../../../../shared/model/ionic-essentials/ionic-essentials.model";
import {EscolaFiltro} from "../../../models/escola-filtro.model";

export class SaveEscolaFiltroStorageCommand extends SimpleCommand implements ICommand {

    /** @override */
    public execute(notification: INotification): void {

        let escolaFiltro: EscolaFiltro = notification.getBody();
        let storage: Storage = IonicEssentials.storage;
        storage
            .ready()
            .then(() => this.onStorageReady(storage, escolaFiltro))
            .catch(() => this.onFailure());

    }

    private onStorageReady(storage: Storage, escolaFiltro: EscolaFiltro): void {

        let data: string = JSON.stringify(escolaFiltro);
        storage
            .set(EscolaConstants.STORAGE_ESCOLA_FILTRO, data)
            .then(() => this.sendNotification(EscolaStorageNotifications.SUCCESS_SAVE_ESCOLA_FILTRO))
            .catch(() => this.onFailure());

    }

    private onFailure(): void {

        this.sendNotification(EscolaStorageNotifications.FAILURE_SAVE_ESCOLA_FILTRO);

    }

}
