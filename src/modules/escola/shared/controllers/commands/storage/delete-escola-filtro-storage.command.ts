import SimpleCommand = puremvc.SimpleCommand;
import ICommand = puremvc.ICommand;
import INotification = puremvc.INotification;
import {Storage} from "@ionic/storage";

import {EscolaStorageNotifications} from "../../../notifications/escola-storage.notifications";
import {EscolaConstants} from "../../../constants/escola.constants";
import {IonicEssentials} from "../../../../../../shared/model/ionic-essentials/ionic-essentials.model";

export class DeleteEscolaFiltroStorageCommand extends SimpleCommand implements ICommand {

    /** @override */
    public execute(notification: INotification): void {

        let storage: Storage = IonicEssentials.storage;
        storage
            .ready()
            .then(() => this.onStorageReady(storage))
            .catch(() => this.onFailure());

    }

    private onStorageReady(storage: Storage): void {

        storage
            .remove(EscolaConstants.STORAGE_ESCOLA_FILTRO)
            .then(() => this.sendNotification(EscolaStorageNotifications.SUCCESS_DELETE_ESCOLA_FILTRO))
            .catch(() => this.onFailure());

    }

    private onFailure(): void {

        this.sendNotification(EscolaStorageNotifications.FAILURE_READ_ESCOLA_FILTRO);

    }

}
