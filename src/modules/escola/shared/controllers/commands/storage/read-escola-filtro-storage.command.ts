import SimpleCommand = puremvc.SimpleCommand;
import ICommand = puremvc.ICommand;
import INotification = puremvc.INotification;
import {Storage} from "@ionic/storage";

import {EscolaStorageNotifications} from "../../../notifications/escola-storage.notifications";
import {EscolaConstants} from "../../../constants/escola.constants";
import {IonicEssentials} from "../../../../../../shared/model/ionic-essentials/ionic-essentials.model";
import {EscolaFiltro} from "../../../models/escola-filtro.model";

export class ReadEscolaFiltroStorageCommand extends SimpleCommand implements ICommand {

    /** @override */
    public execute(notification: INotification): void {

        let storage: Storage = IonicEssentials.storage;
        storage
            .ready()
            .then(() => this.onStorageReady(storage))
            .catch(() => this.onFailure());

    }

    private onStorageReady(storage: Storage): void {

        storage
            .get(EscolaConstants.STORAGE_ESCOLA_FILTRO)
            .then((data: string) => this.onDataReady(data))
            .catch(() => this.onFailure());

    }

    private onDataReady(data: string): void {

        let dataStorage: Object[] = JSON.parse(data || "{}");
        let escolaFiltro: EscolaFiltro = new EscolaFiltro(dataStorage);

        this.sendNotification(EscolaStorageNotifications.SUCCESS_READ_ESCOLA_FILTRO, escolaFiltro);

    }

    private onFailure(): void {

        this.sendNotification(EscolaStorageNotifications.FAILURE_READ_ESCOLA_FILTRO);

    }

}
