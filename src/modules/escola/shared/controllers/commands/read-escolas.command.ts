import SimpleCommand = puremvc.SimpleCommand;
import ICommand = puremvc.ICommand;
import INotification = puremvc.INotification;
import IFacade = puremvc.IFacade;
import {Observable} from "rxjs/Observable";
import {Response} from '@angular/http';

import {AppFacade} from "../../../../../shared/core/application-facade";
import {EscolaProxy} from "../../models/escola.proxy";
import {EscolaFiltro} from "../../models/escola-filtro.model";
import {EscolaFactory} from "../../models/escola.factory";
import {Escola} from "../../models/escola.model";
import {EscolaNotifications} from "../../notifications/escola.notifications";
import {Result} from "../../../../../shared/model/result/result.model";

export class ReadEscolasCommand extends SimpleCommand implements ICommand {

    /** @override */
    public execute(notification: INotification): void {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
        let proxy: EscolaProxy = facade.retrieveProxy(EscolaProxy.NAME) as EscolaProxy;

        let escolaFiltro: EscolaFiltro = notification.getBody();
        let observer: Observable<Object> = proxy.readEscolas(escolaFiltro);
        observer.subscribe(
            (data: Object) => this.onSuccessReadEscolas(data),
            (response: Response) => this.onFailureReadEscolas(response)
        );

    }

    private onSuccessReadEscolas(data: Object): void {

        let response: Object[] = data['escolas'] || [];
        let factory: EscolaFactory = new EscolaFactory();
        let escolas: Escola[] = factory.create(response);

        this.sendNotification(EscolaNotifications.SUCCESS_READ_ESCOLAS, escolas);

    }

    private onFailureReadEscolas(response: Response): void {

        let result: Result = new Result();
        result.response = response;

        this.sendNotification(EscolaNotifications.FAILURE_READ_ESCOLAS, result);

    }

}
