export class EscolaStorageNotifications {

    public static SAVE_ESCOLA_FILTRO: string = "EscolaStorageNotifications.SAVE_ESCOLA_FILTRO";
    public static SUCCESS_SAVE_ESCOLA_FILTRO: string = "EscolaStorageNotifications.SUCCESS_SAVE_ESCOLA_FILTRO";
    public static FAILURE_SAVE_ESCOLA_FILTRO: string = "EscolaStorageNotifications.FAILURE_SAVE_ESCOLA_FILTRO";

    public static READ_ESCOLA_FILTRO: string = "EscolaStorageNotifications.READ_ESCOLA_FILTRO";
    public static SUCCESS_READ_ESCOLA_FILTRO: string = "EscolaStorageNotifications.SUCCESS_READ_ESCOLA_FILTRO";
    public static FAILURE_READ_ESCOLA_FILTRO: string = "EscolaStorageNotifications.FAILURE_READ_ESCOLA_FILTRO";

    public static DELETE_ESCOLA_FILTRO: string = "EscolaStorageNotifications.DELETE_ESCOLA_FILTRO";
    public static SUCCESS_DELETE_ESCOLA_FILTRO: string = "EscolaStorageNotifications.SUCCESS_DELETE_ESCOLA_FILTRO";
    public static FAILURE_DELETE_ESCOLA_FILTRO: string = "EscolaStorageNotifications.FAILURE_DELETE_ESCOLA_FILTRO";

}
