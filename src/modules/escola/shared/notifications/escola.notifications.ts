export class EscolaNotifications {

    public static SAVE_ESCOLAS: string = "EscolaNotifications.SAVE_ESCOLAS";
    public static SUCCESS_SAVE_ESCOLAS: string = "EscolaNotifications.SUCCESS_SAVE_ESCOLAS";
    public static FAILURE_SAVE_ESCOLAS: string = "EscolaNotifications.FAILURE_SAVE_ESCOLAS";

    public static READ_ESCOLAS: string = "EscolaNotifications.READ_ESCOLAS";
    public static SUCCESS_READ_ESCOLAS: string = "EscolaNotifications.SUCCESS_READ_ESCOLAS";
    public static FAILURE_READ_ESCOLAS: string = "EscolaNotifications.FAILURE_READ_ESCOLAS";

}
