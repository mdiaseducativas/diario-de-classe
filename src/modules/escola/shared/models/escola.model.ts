import {Model} from '../../../../shared/model/base.model';
import {Turma} from '../../../turma/shared/models/turma.model';

export class Escola extends Model {

    id_escola: string;
    nome: string;
    foto: string;
    turmas: Turma[];

    constructor(data:Object = {}) {

        super();

        this.turmas = [];

        this.set(data);

    }

}
