import IProxy = puremvc.IProxy;
import Proxy = puremvc.Proxy;
import {Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import {EscolaFiltro} from "./escola-filtro.model";
import {EscolaService} from "../services/escola.service";

export class EscolaProxy extends Proxy implements IProxy {

    public static NAME: string = "EscolaProxy.NAME";

    public service: EscolaService;

    constructor(data?: any) {

        super(EscolaProxy.NAME, data);

    }

    public readEscolas(escolaFiltro: EscolaFiltro): Observable<Object> {

        let observer: Observable<Response> = this.service.readEscolas(escolaFiltro);
        return observer
            .map((res: Response) => res.json())
            .map((data: Object) => data['data']);

    }

}
