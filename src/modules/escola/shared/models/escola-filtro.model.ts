import {Model} from '../../../../shared/model/base.model';
import {Turma} from '../../../turma/shared/models/turma.model';
import {Escola} from "./escola.model";
import {AnoLetivo} from "../../../ano-letivo/shared/models/ano-letivo.model";
import {Disciplina} from "../../../../shared/model/disciplina/disciplina.model";

export class EscolaFiltro extends Model {

    anoLetivo: AnoLetivo;
    escola: Escola;
    turma: Turma;
    disciplina: Disciplina;

    constructor(data: Object = {}) {
        super();
        this.set(data);
    }

    isValid(): boolean {

        return Boolean(this.anoLetivo && this.escola && this.turma && this.disciplina);

    }

}
