import {Escola} from "./escola.model";
import {TurmaFactory} from "../../../turma/shared/models/turma.factory";

export class EscolaFactory {

    public create(data: any): any {

        switch (data.constructor) {
            case Array: {
                return this.createMany(data);
            }

            case Object: {
                return this.createOne(data);
            }
        }

    }

    private createOne(data: Object): Escola {

        let escola: Escola = new Escola(data);
        if (escola.turmas) {
            let turmaFactory: TurmaFactory = new TurmaFactory();
            escola.turmas = turmaFactory.create(escola.turmas);
        }

        return escola;

    }

    private createMany(data: Object[]): Escola[] {

        let escolas: Escola[] = [];

        for (let i: number = 0; i < data.length; i++) {
            let escola: Escola = this.createOne(data[i]);
            escolas.push(escola);
        }

        return escolas;

    }

}
