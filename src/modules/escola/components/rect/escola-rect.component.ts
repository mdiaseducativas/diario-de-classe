import {Component, EventEmitter, Output} from '@angular/core';

import {Escola} from "../../shared/models/escola.model";
import {AnoLetivo} from "../../../ano-letivo/shared/models/ano-letivo.model";
import {Turma} from "../../../turma/shared/models/turma.model";
import {EscolaFiltro} from "../../shared/models/escola-filtro.model";

@Component({
    selector: 'escola-rect-component',
    templateUrl: 'escola-rect.component.html',
    providers: []
})

export class EscolaRectComponent {

    @Output() public onClick: EventEmitter<EscolaFiltro>;

    protected escola: Escola;
    protected anoLetivo: AnoLetivo;
    protected turma: Turma;

    constructor() {

        this.escola = null;
        this.anoLetivo = null;
        this.turma = null;

        this.onClick = new EventEmitter<EscolaFiltro>();

    }

    public setEscola(escola: Escola): void {

        this.escola = escola;

    }

    public setTurma(turma: Turma): void {

        this.turma = turma;

    }

    public setAnoLetivo(anoLetivo: AnoLetivo): void {

        this.anoLetivo = anoLetivo;

    }

    protected click(): void {

        let escolaFiltro: EscolaFiltro = new EscolaFiltro();
        escolaFiltro.turma = this.turma;
        escolaFiltro.anoLetivo = this.anoLetivo;
        escolaFiltro.escola = this.escola;

        this.onClick.emit(escolaFiltro);

    }

}
