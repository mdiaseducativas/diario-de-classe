import IFacade = puremvc.IFacade;
import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';

import {EscolaFiltro} from "../../shared/models/escola-filtro.model";
import {ModalController} from "ionic-angular";
import {EscolaRectComponent} from "../rect/escola-rect.component";
import {EscolaRectContainerMediator} from "./shared/escola-rect-container.mediator";
import {AppFacade} from "../../../../shared/core/application-facade";

@Component({
    selector: 'escola-rect-container-component',
    templateUrl: 'escola-rect-container.component.html',
    providers: []
})

export class EscolaRectContainerComponent implements OnInit, OnDestroy {

    @Output()
    public onChangeEscola: EventEmitter<EscolaFiltro>;

    @Input()
    public preview: boolean;

    @ViewChild(EscolaRectComponent)
    public escolaRectComponent: EscolaRectComponent;

    private escolaRectContainerMediator: EscolaRectContainerMediator;
    private modalController: ModalController;

    constructor(modalController: ModalController) {

        this.onChangeEscola = new EventEmitter<EscolaFiltro>();
        this.modalController = modalController;
        this.preview = false;

    }

    public ngOnInit(): void {

        this.escolaRectContainerMediator = new EscolaRectContainerMediator(this, this.modalController);

    }

    public ngOnDestroy(): void {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);

        this.escolaRectContainerMediator.removeAllSubscribers();

        if (facade.hasMediator(EscolaRectContainerMediator.NAME)) {
            facade.removeMediator(EscolaRectContainerMediator.NAME);
        }

    }

}
