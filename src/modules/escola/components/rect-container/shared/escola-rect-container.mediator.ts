import {ModalController} from "ionic-angular";

import {EscolaRectComponent} from "../../rect/escola-rect.component";
import {BaseMediator} from "../../../../../shared/core/base.mediator";
import {EscolaStorageNotifications} from "../../../shared/notifications/escola-storage.notifications";
import {EscolaFiltroModalComponent} from "../../filtro-modal/escola-filtro-modal.component";
import {EscolaRectContainerComponent} from "../escola-rect-container.component";
import {EscolaFiltro} from "../../../shared/models/escola-filtro.model";

export class EscolaRectContainerMediator extends BaseMediator {

    public static NAME: string = 'EscolaRectContainerMediator.NAME';

    private escolaFiltro: EscolaFiltro;
    private escolaRectContainerComponent: EscolaRectContainerComponent;
    private escolaRectComponent: EscolaRectComponent;
    private modalController: ModalController;

    constructor(escolaRectContainerComponent: EscolaRectContainerComponent, modalController: ModalController) {

        super(EscolaRectContainerMediator.NAME);

        this.escolaFiltro = new EscolaFiltro();
        this.escolaRectContainerComponent = escolaRectContainerComponent;
        this.escolaRectComponent = escolaRectContainerComponent.escolaRectComponent;
        this.modalController = modalController;

        this.subscribe(this.escolaRectComponent.onClick, this.onClickEscola);

        this.readEscolaFiltro();

    }

    private readEscolaFiltro(): void {

        this.sendNotification(EscolaStorageNotifications.READ_ESCOLA_FILTRO);

    }

    private onClickEscola(): void {

        if (this.escolaRectContainerComponent.preview) {
            return;
        }

        let modal = this.modalController.create(EscolaFiltroModalComponent);
        modal.present();
        modal.onDidDismiss((change: boolean) => this.onModalDidDimiss(change));

    }

    private onModalDidDimiss(change: boolean): void {

        this.readEscolaFiltro();

        let escolaFiltro: EscolaFiltro = null;
        if (change) {
            escolaFiltro = this.escolaFiltro;
        }

        this.escolaRectContainerComponent.onChangeEscola.emit(escolaFiltro);

    }

    /** @override */
    public registerListeners(): void {

        this.addListener(EscolaStorageNotifications.SUCCESS_READ_ESCOLA_FILTRO, this.onSuccessReadEscolaFiltro);
        this.addListener(EscolaStorageNotifications.FAILURE_READ_ESCOLA_FILTRO, this.onFailureReadEscolaFiltro);

    }

    private onSuccessReadEscolaFiltro(escolaFiltro: EscolaFiltro): void {

        this.escolaFiltro = escolaFiltro;

        this.escolaRectComponent.setEscola(escolaFiltro.escola);
        this.escolaRectComponent.setTurma(escolaFiltro.turma);
        this.escolaRectComponent.setAnoLetivo(escolaFiltro.anoLetivo);

    }

    private onFailureReadEscolaFiltro(): void {

    }

}
