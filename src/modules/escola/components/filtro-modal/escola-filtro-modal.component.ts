import {NavParams, ViewController} from 'ionic-angular';
import {AfterViewInit, Component, forwardRef, OnDestroy, ViewChild} from '@angular/core';
import {Subscriber} from "rxjs/Subscriber";

import {EscolaFiltro} from "../../shared/models/escola-filtro.model";
import {EscolaPage} from "../../page/escola.page";

@Component({
    selector: 'escola-filtro-modal-component',
    templateUrl: 'escola-filtro-modal.component.html',
    providers: []
})

export class EscolaFiltroModalComponent implements AfterViewInit, OnDestroy {

    @ViewChild(forwardRef(() => EscolaPage)) escolaPage: EscolaPage;

    private params: NavParams;
    private viewController: ViewController;
    private subscribe: Subscriber<EscolaFiltro>;

    constructor(params: NavParams, viewController: ViewController) {

        this.params = params;
        this.viewController = viewController;

    }

    public ngAfterViewInit(): void {

        this.subscribe = this.escolaPage.onChangeEscolaFiltro.subscribe(() => this.dimiss(true));

    }

    public ngOnDestroy(): void {

        this.subscribe.unsubscribe();

    }

    protected dimiss(change: boolean): void {

        this.viewController.dismiss(change);

    }

}
