import {Component, EventEmitter, Output} from '@angular/core';

import {Escola} from "../../shared/models/escola.model";

@Component({
    selector: 'escola-grid-component',
    templateUrl: 'escola-grid.component.html',
    providers: []
})

export class EscolaGridComponent {

    @Output() public onSelectEscola: EventEmitter<Escola>;

    protected escola: Escola;
    protected escolas: Escola[];

    constructor() {

        this.escolas = [];
        this.escola = new Escola();
        this.onSelectEscola = new EventEmitter<Escola>();

    }

    public setEscolas(escolas: Escola[]): void {

        this.escolas = escolas;

    }

    public setEscola(escola: Escola): void {

        this.escola = escola;

    }

    protected selectEscola(escola: Escola): void {

        this.setEscola(escola);

        this.onSelectEscola.emit(escola);

    }

}
