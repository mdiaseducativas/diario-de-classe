import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ModalController, NavController} from "ionic-angular";

import {Page} from "../../../shared/core/base.page";
import {FrequenciaHorariosMediator} from "./shared/mediators/frequencia-horarios.mediator";
import {EscolaFiltro} from "../../escola/shared/models/escola-filtro.model";
import {FrequenciaUsuariosMediator} from "./shared/mediators/frequencia-usuarios.mediator";
import {UsuarioGridComponent} from "../../usuario/components/grid/usuario-grid.component";
import {EscolaRectContainerComponent} from "../../escola/components/rect-container/escola-rect-container.component";
import {FrequenciaEscolaMediator} from "./shared/mediators/frequencia-escola.mediator";
import {HorariosRectContainerComponent} from "../../horarios/components/rect-container/horarios-rect-container.component";

@Component({
    selector: 'frequencia-page',
    templateUrl: 'frequencia.page.html',
    providers: []
})

export class FrequenciaPage extends Page implements OnInit, AfterViewInit {

    @ViewChild(UsuarioGridComponent)
    public usuarioGridComponent: UsuarioGridComponent;

    @ViewChild(EscolaRectContainerComponent)
    public escolaRectContainerComponent: EscolaRectContainerComponent;

    @ViewChild(HorariosRectContainerComponent)
    public horariosRectContainerComponent: HorariosRectContainerComponent;

    public escolaFiltro: EscolaFiltro;
    public navController: NavController;
    public modalController: ModalController;
    public frequenciaHorariosMediator: FrequenciaHorariosMediator;
    public frequenciaUsuariosMediator: FrequenciaUsuariosMediator;
    public frequenciaEscolaMediator: FrequenciaEscolaMediator;

    constructor(navController: NavController, modalController: ModalController) {

        super();

        this.navController = navController;
        this.modalController = modalController;

    }

    public ngOnInit(): void {

        this.frequenciaHorariosMediator = new FrequenciaHorariosMediator(this);
        this.frequenciaEscolaMediator = new FrequenciaEscolaMediator(this);
        this.frequenciaUsuariosMediator = new FrequenciaUsuariosMediator(this);

        this.addMediator(this.frequenciaHorariosMediator);
        this.addMediator(this.frequenciaEscolaMediator);
        this.addMediator(this.frequenciaUsuariosMediator);

    }

    public ngAfterViewInit(): void {

        this.frequenciaUsuariosMediator.readUsuarios();

    }

}
