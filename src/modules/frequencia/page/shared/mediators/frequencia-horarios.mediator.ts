import {BaseMediator} from "../../../../../shared/core/base.mediator";
import {FrequenciaPage} from "../../frequencia.page";
import {FrequenciaHorariosRectComponent} from "../../../components/horarios-rect/frequencia-horarios-rect.component";
import {ModalController} from "ionic-angular";
import {FrequenciaHorariosModalComponent} from "../../../components/horarios-modal/frequencia-horarios-modal.component";
import {ConvertDateDBUtil} from "../../../../../shared/utils/convert-date-db.util";
import {Horario} from "../../../../horarios/shared/models/horario.model";
import {EscolaFiltro} from "../../../../escola/shared/models/escola-filtro.model";
import {FrequenciaUsuariosMediator} from "./frequencia-usuarios.mediator";
import {LoginNotifications} from "../../../../login/shared/notifications/login.notification";
import {HorariosRectContainerComponent} from "../../../../horarios/components/rect-container/horarios-rect-container.component";
import {EscolaStorageNotifications} from "../../../../escola/shared/notifications/escola-storage.notifications";
import {Turma} from "../../../../turma/shared/models/turma.model";

export class FrequenciaHorariosMediator extends BaseMediator {

    public static NAME: string = 'FrequenciaHorariosMediator.NAME';

    private frequenciaPage: FrequenciaPage;
    private horariosRectContainerComponent: HorariosRectContainerComponent;

    constructor(frequenciaPage: FrequenciaPage) {

        super(FrequenciaHorariosMediator.NAME);

        this.frequenciaPage = frequenciaPage;
        this.horariosRectContainerComponent = frequenciaPage.horariosRectContainerComponent;

        this.subscribe(this.horariosRectContainerComponent.onChange, this.onChangeHorarios);

    }

    private onChangeHorarios(): void {

        this.frequenciaPage.frequenciaUsuariosMediator.readUsuarios();

    }

    /** @override */
    public registerListeners(): void {

        this.addListener(EscolaStorageNotifications.SUCCESS_READ_ESCOLA_FILTRO, this.onSuccessReadEscolaFiltro);

    }

    private onSuccessReadEscolaFiltro(escolaFiltro: EscolaFiltro): void {

        this.horariosRectContainerComponent.setEscolaFiltro(escolaFiltro);

    }

}
