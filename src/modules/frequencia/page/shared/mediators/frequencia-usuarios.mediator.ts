import {BaseMediator} from "../../../../../shared/core/base.mediator";
import {FrequenciaPage} from "../../frequencia.page";
import {UsuarioNotifications} from "../../../../usuario/shared/notifications/usuario.notifications";
import {UsuarioFiltro} from "../../../../usuario/shared/models/usuario-filtro.model";
import {Usuario} from "../../../../usuario/shared/models/usuario.model";
import {UsuarioGridComponent} from "../../../../usuario/components/grid/usuario-grid.component";
import {FrequenciaHorariosRectComponent} from "../../../components/horarios-rect/frequencia-horarios-rect.component";
import {ConvertDateDBUtil} from "../../../../../shared/utils/convert-date-db.util";
import {HorariosRectContainerComponent} from "../../../../horarios/components/rect-container/horarios-rect-container.component";
import {EscolaFiltro} from "../../../../escola/shared/models/escola-filtro.model";
import {HorariosRectComponent} from "../../../../horarios/components/rect/horarios-rect.component";

export class FrequenciaUsuariosMediator extends BaseMediator {

    public static NAME: string = 'FrequenciaUsuariosMediator.NAME';

    private frequenciaPage: FrequenciaPage;
    private usuarioGridComponent: UsuarioGridComponent;
    private horariosRectContainerComponent: HorariosRectContainerComponent;
    private usuarioFiltro: UsuarioFiltro;

    constructor(frequenciaPage: FrequenciaPage) {

        super(FrequenciaUsuariosMediator.NAME);

        this.frequenciaPage = frequenciaPage;
        this.horariosRectContainerComponent = frequenciaPage.horariosRectContainerComponent;
        this.usuarioGridComponent = frequenciaPage.usuarioGridComponent;
        this.usuarioFiltro = new UsuarioFiltro();

        this.subscribe(this.usuarioGridComponent.onSelectUsuario, this.onSelectUsuario);
        this.subscribe(frequenciaPage.horariosRectContainerComponent.onChange, this.readUsuarios);

    }

    private onSelectUsuario(usuario: Usuario): void {

        usuario.frequencia = Usuario.mudarFrequencia(usuario.frequencia);

        this.sendNotification(UsuarioNotifications.CHANGE_FREQUENCIA_USUARIO, usuario);

    }

    public readUsuarios(): void {

        this.usuarioGridComponent.setLoading(true);

        let horariosRectComponent: HorariosRectComponent = this.horariosRectContainerComponent.horariosRectComponent;
        let data: string = horariosRectComponent.getData().trim();

        this.usuarioFiltro.data = ConvertDateDBUtil.convertDateToDB(data);
        this.usuarioFiltro.horarios = horariosRectComponent.getHorarios();

        this.sendNotification(UsuarioNotifications.READ_FREQUENCIA_USUARIOS, this.usuarioFiltro);

    }

    /** @override */
    public registerListeners(): void {

        this.addListener(UsuarioNotifications.SUCCESS_READ_FREQUENCIA_USUARIOS, this.onSuccessReadUsuarios);
        this.addListener(UsuarioNotifications.FAILURE_READ_FREQUENCIA_USUARIOS, this.onFailureReadUsuarios);

    }

    private onSuccessReadUsuarios(usuarios: Usuario[]): void {

        this.usuarioGridComponent.setLoading(false);
        this.usuarioGridComponent.setUsuarios(usuarios);

    }

    private onFailureReadUsuarios(): void {

        this.usuarioGridComponent.setLoading(false);
        this.usuarioGridComponent.setUsuarios([]);

    }

}
