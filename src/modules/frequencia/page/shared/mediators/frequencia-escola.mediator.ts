import {ModalController} from "ionic-angular";

import {BaseMediator} from "../../../../../shared/core/base.mediator";
import {FrequenciaPage} from "../../frequencia.page";
import {EscolaFiltro} from "../../../../escola/shared/models/escola-filtro.model";
import {EscolaRectContainerComponent} from "../../../../escola/components/rect-container/escola-rect-container.component";
import {HorariosRectComponent} from "../../../../horarios/components/rect/horarios-rect.component";

export class FrequenciaEscolaMediator extends BaseMediator {

    public static NAME: string = 'FrequenciaEscolaMediator.NAME';

    private frequenciaPage: FrequenciaPage;
    private modalController: ModalController;
    private escolaRectContainerComponent: EscolaRectContainerComponent;

    constructor(frequenciaPage: FrequenciaPage) {

        super(FrequenciaEscolaMediator.NAME);

        this.frequenciaPage = frequenciaPage;
        this.escolaRectContainerComponent = frequenciaPage.escolaRectContainerComponent;
        this.modalController = frequenciaPage.modalController;

        this.subscribe(this.escolaRectContainerComponent.onChangeEscola, this.onChangeEscola);

    }

    private onChangeEscola(escolaFiltro: EscolaFiltro): void {

        if(!escolaFiltro) {
            return;
        }

        this.frequenciaPage.escolaFiltro = escolaFiltro;
        this.frequenciaPage.horariosRectContainerComponent.reset();
        this.frequenciaPage.frequenciaUsuariosMediator.readUsuarios();

    }

}
