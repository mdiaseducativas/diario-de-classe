import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {SharedModule} from "../../components/shared.module";
import {FrequenciaPage} from "./page/frequencia.page";
import {UsuarioModule} from "../usuario/usuario.module";
import {EscolaModule} from "../escola/escola.module";
import {HorariosModule} from "../horarios/horarios.module";

@NgModule({
    declarations: [
        FrequenciaPage
    ],
    entryComponents: [
        FrequenciaPage
    ],
    exports: [],
    imports: [
        UsuarioModule,
        EscolaModule,
        HorariosModule,
        IonicModule.forRoot(FrequenciaPage),
        SharedModule
    ],
    bootstrap: [IonicApp],
    providers: [
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        }
    ]
})

export class FrequenciaModule {

}
