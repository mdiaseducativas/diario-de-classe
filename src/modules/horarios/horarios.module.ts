import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {SharedModule} from "../../components/shared.module";
import {HorariosRectComponent} from "./components/rect/horarios-rect.component";
import {HorariosModalComponent} from "./components/modal/horarios-modal.component";
import {HorariosRectContainerComponent} from "./components/rect-container/horarios-rect-container.component";

@NgModule({
    declarations: [
        HorariosRectComponent,
        HorariosModalComponent,
        HorariosRectContainerComponent
    ],
    entryComponents: [
        HorariosRectComponent,
        HorariosModalComponent,
        HorariosRectContainerComponent
    ],
    exports: [
        HorariosRectComponent,
        HorariosModalComponent,
        HorariosRectContainerComponent
    ],
    imports: [
        IonicModule.forRoot(HorariosRectContainerComponent),
        SharedModule
    ],
    bootstrap: [IonicApp],
    providers: [
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        }
    ]
})

export class HorariosModule {

    constructor() {

    }

}
