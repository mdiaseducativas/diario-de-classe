import IFacade = puremvc.IFacade;
import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {ModalController} from "ionic-angular";

import {HorariosRectComponent} from "../rect/horarios-rect.component";
import {HorariosRectContainerMediator} from "./shared/horarios-rect-container.mediator";
import {AppFacade} from "../../../../shared/core/application-facade";
import {EscolaFiltro} from "../../../escola/shared/models/escola-filtro.model";

@Component({
    selector: 'horarios-rect-container-component',
    templateUrl: 'horarios-rect-container.component.html',
    providers: []
})

export class HorariosRectContainerComponent implements OnInit, OnDestroy {

    @ViewChild(HorariosRectComponent)
    public horariosRectComponent: HorariosRectComponent;

    @Output()
    public onChange: EventEmitter<any>;

    public escolaFiltro: EscolaFiltro;

    private modalController: ModalController;
    private horariosRectContainerMediator: HorariosRectContainerMediator;

    constructor(modalController: ModalController) {

        this.onChange = new EventEmitter<any>();
        this.modalController = modalController;
        this.escolaFiltro = new EscolaFiltro();

    }

    public ngOnInit(): void {

        this.horariosRectContainerMediator = new HorariosRectContainerMediator(this, this.modalController);

        this.reset();

    }

    public setEscolaFiltro(escolaFiltro: EscolaFiltro): void {

        this.escolaFiltro = escolaFiltro;

    }

    public reset(): void {

        this.horariosRectContainerMediator.setInitialValues();

    }

    public ngOnDestroy(): void {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);

        this.horariosRectContainerMediator.removeAllSubscribers();

        if (facade.hasMediator(HorariosRectContainerMediator.NAME)) {
            facade.removeMediator(HorariosRectContainerMediator.NAME);
        }

    }

}
