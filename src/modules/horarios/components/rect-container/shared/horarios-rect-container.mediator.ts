import {ModalController} from "ionic-angular";
import {BaseMediator} from "../../../../../shared/core/base.mediator";
import {HorariosRectComponent} from "../../rect/horarios-rect.component";
import {ConvertDateDBUtil} from "../../../../../shared/utils/convert-date-db.util";
import {HorariosModalComponent} from "../../modal/horarios-modal.component";
import {Horario} from "../../../shared/models/horario.model";
import {Turma} from "../../../../turma/shared/models/turma.model";
import {HorariosRectContainerComponent} from "../horarios-rect-container.component";

export class HorariosRectContainerMediator extends BaseMediator {

    public static NAME: string = 'HorariosRectContainerMediator.NAME';

    private horariosRectContainerComponent: HorariosRectContainerComponent;
    private horariosRectComponent: HorariosRectComponent;
    private modalController: ModalController;

    constructor(horariosRectContainerComponent: HorariosRectContainerComponent, modalController: ModalController) {

        super(HorariosRectContainerMediator.NAME);

        this.horariosRectContainerComponent = horariosRectContainerComponent;
        this.horariosRectComponent = horariosRectContainerComponent.horariosRectComponent;
        this.modalController = modalController;

        this.subscribe(this.horariosRectComponent.onClick, this.onClickHorarios);

    }

    public setInitialValues(): void {

        let data: string = ConvertDateDBUtil.convertDateToString(new Date(), false);
        data = ConvertDateDBUtil.convertDateFromDB(data);

        this.horariosRectComponent.setData(data);
        this.horariosRectComponent.setHorarios([]);

    }

    private onClickHorarios(): void {

        let data: string = this.horariosRectComponent.getData();
        let horarios: Horario[] = this.horariosRectComponent.getHorarios();
        let turma: Turma = this.horariosRectContainerComponent.escolaFiltro.turma;

        if (!turma) {
            console.error('Turma não foi setada');
            return;
        }

        let modal = this.modalController.create(HorariosModalComponent, {
            turma: turma,
            horarios: horarios,
            data: data
        });

        modal.present();

        modal.onDidDismiss(response => this.onModalDidDimiss(response));

    }

    private onModalDidDimiss(response: { horarios: Horario[], data: string }): void {

        if (!response) {
            return;
        }

        let horarios: Horario[] = response.horarios;
        let data: string = response.data;

        this.horariosRectComponent.setData(data);
        this.horariosRectComponent.setHorarios(horarios);
        this.horariosRectContainerComponent.onChange.emit(response);

    }

}
