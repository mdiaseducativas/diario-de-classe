import {Component, EventEmitter, Output} from '@angular/core';
import {Horario} from "../../shared/models/horario.model";

@Component({
    selector: 'horarios-rect-component',
    templateUrl: 'horarios-rect.component.html',
    providers: []
})

export class HorariosRectComponent {

    @Output() onClick: EventEmitter<null>;

    protected data: string;
    protected horarios: Horario[];

    constructor() {

        this.horarios = [];

        this.onClick = new EventEmitter<null>();

    }

    public setHorarios(horarios: Horario[]): void {

        this.horarios = horarios;

    }

    public getHorarios(): Horario[] {

        return this.horarios;

    }

    public setData(data: string): void {

        this.data = data;

    }

    public getData(): string {

        return this.data;

    }

    protected click(): void {

        this.onClick.emit();

    }

}
