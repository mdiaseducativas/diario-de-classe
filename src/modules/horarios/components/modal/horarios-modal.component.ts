import {NavParams, ViewController} from 'ionic-angular';
import {Component} from '@angular/core';
import {Turma} from "../../../turma/shared/models/turma.model";
import {ConvertDateDBUtil} from "../../../../shared/utils/convert-date-db.util";
import {Horario} from "../../shared/models/horario.model";

@Component({
    selector: 'horarios-modal-component',
    templateUrl: 'horarios-modal.component.html',
    providers: []
})

export class HorariosModalComponent {

    private params: NavParams;
    private viewController: ViewController;

    protected data: string;
    protected turma: Turma;
    protected horarios: boolean[];

    constructor(params: NavParams, viewController: ViewController) {

        this.horarios = [];
        this.params = params;
        this.viewController = viewController;
        this.turma = params.get('turma') || {};
        this.data = this.getInitialDate();

        this.getInitialBooleanHorarios();

    }

    private getInitialDate(): string {

        let dataFormatada: string = this.params.get('data');
        let date: Date = ConvertDateDBUtil.convertFormatedDateToDate(dataFormatada);

        return date.toISOString();

    }

    private getInitialBooleanHorarios(): void {

        let horarios: Horario[] = this.params.get('horarios') || [];

        for (let i = 0; i < this.turma.horarios.length; i++) {
            this.horarios[i] = false;

            for (let j = 0; j < horarios.length; j++) {
                if (this.turma.horarios[i].id_horario == horarios[j].id_horario) {
                    this.horarios[i] = true;
                }
            }
        }

    }

    private getHorarios(): Horario[] {

        let horarios: Horario[] = [];

        for (let i = 0; i < this.horarios.length; i++) {
            if (this.horarios[i]) {
                horarios.push(this.turma.horarios[i]);
            }
        }

        return horarios;

    }

    protected dimiss(sendParams: boolean): void {

        let date: Date = new Date(this.data);
        let dataDB: string = ConvertDateDBUtil.convertDateToString(date);
        let dataFormatada: string = ConvertDateDBUtil.convertDateFromDB(dataDB);
        let horarios = this.getHorarios();
        let data: Object = {
            data: dataFormatada,
            horarios: horarios,
            turma: this.turma
        };

        if (!sendParams) {
            data = null;
        }

        this.viewController.dismiss(data);

    }

}
