import {Model} from '../../../../shared/model/base.model';

export class Horario extends Model {

    id_horario: string;
    nome: string;

    constructor(data: Object = {}) {
        super();
        this.set(data);
    }

}
