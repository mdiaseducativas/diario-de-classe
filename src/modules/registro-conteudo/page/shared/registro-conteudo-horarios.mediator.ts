import {BaseMediator} from "../../../../shared/core/base.mediator";
import {RegistroConteudoPage} from "../registro-conteudo.page";
import {HorariosRectContainerComponent} from "../../../horarios/components/rect-container/horarios-rect-container.component";
import {EscolaStorageNotifications} from "../../../escola/shared/notifications/escola-storage.notifications";
import {EscolaFiltro} from "../../../escola/shared/models/escola-filtro.model";

export class RegistroConteudoHorariosMediator extends BaseMediator {

    public static NAME: string = 'RegistroConteudoHorariosMediator.NAME';

    private registroConteudoPage: RegistroConteudoPage;
    private horariosRectContainerComponent: HorariosRectContainerComponent;

    constructor(registroConteudoPage: RegistroConteudoPage) {

        super(RegistroConteudoHorariosMediator.NAME);

        this.registroConteudoPage = registroConteudoPage;
        this.horariosRectContainerComponent = registroConteudoPage.horariosRectContainerComponent;

        this.subscribe(this.horariosRectContainerComponent.onChange, this.onChangeHorarios);

    }

    private onChangeHorarios(): void {

        this.registroConteudoPage.registroConteudoMediator.readRegistrosConteudos();

    }

    /** @override */
    public registerListeners(): void {

        this.addListener(EscolaStorageNotifications.SUCCESS_READ_ESCOLA_FILTRO, this.onSuccessReadEscolaFiltro);

    }

    private onSuccessReadEscolaFiltro(escolaFiltro: EscolaFiltro): void {

        this.horariosRectContainerComponent.setEscolaFiltro(escolaFiltro);

    }

}
