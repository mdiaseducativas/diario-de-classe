import {BaseMediator} from "../../../../shared/core/base.mediator";
import {RegistroConteudoPage} from "../registro-conteudo.page";
import {EscolaRectContainerComponent} from "../../../escola/components/rect-container/escola-rect-container.component";
import {EscolaFiltro} from "../../../escola/shared/models/escola-filtro.model";

export class RegistroConteudoEscolaMediator extends BaseMediator {

    public static NAME: string = 'RegistroConteudoEscolaMediator.NAME';

    private registroConteudoPage: RegistroConteudoPage;
    private escolaRectContainerComponent: EscolaRectContainerComponent;

    constructor(registroConteudoPage: RegistroConteudoPage) {

        super(RegistroConteudoEscolaMediator.NAME);

        this.registroConteudoPage = registroConteudoPage;
        this.escolaRectContainerComponent = registroConteudoPage.escolaRectContainerComponent;

        this.subscribe(this.escolaRectContainerComponent.onChangeEscola, this.onChangeEscola);

    }

    private onChangeEscola(escolaFiltro: EscolaFiltro): void {

        if (!escolaFiltro) {
            return;
        }

        this.registroConteudoPage.horariosRectContainerComponent.reset();
        this.registroConteudoPage.registroConteudoMediator.readRegistrosConteudos();

    }

}
