import {Response} from '@angular/http';

import {BaseMediator} from "../../../../shared/core/base.mediator";
import {RegistroConteudoPage} from "../registro-conteudo.page";
import {RegistroConteudoNotifications} from "../../shared/notifications/registro-conteudo.notifications";
import {RegistroConteudo} from "../../shared/models/registro-conteudo.model";
import {ListaRegistrosConteudosComponent} from "../../components/lista-registros-conteudos/lista-registros-conteudos.component";
import {RegistroConteudoFiltro} from "../../shared/models/registro-conteudo-filtro.model";
import {HorariosRectComponent} from "../../../horarios/components/rect/horarios-rect.component";
import {HorariosRectContainerComponent} from "../../../horarios/components/rect-container/horarios-rect-container.component";
import {ConvertDateDBUtil} from "../../../../shared/utils/convert-date-db.util";

export class RegistroConteudoMediator extends BaseMediator {

    public static NAME: string = 'RegistroConteudoMediator.NAME';

    private registroConteudoPage: RegistroConteudoPage;

    constructor(registroConteudoPage: RegistroConteudoPage) {

        super(RegistroConteudoMediator.NAME);

        this.registroConteudoPage = registroConteudoPage;

    }

    public readRegistrosConteudos(): void {

        this.registroConteudoPage.setLoading(true);

        let registroConteudoFiltro: RegistroConteudoFiltro = new RegistroConteudoFiltro();
        let horariosRectContainerComponent: HorariosRectContainerComponent = this.registroConteudoPage.horariosRectContainerComponent;
        let horariosRectComponent: HorariosRectComponent = horariosRectContainerComponent.horariosRectComponent;
        let data: string = horariosRectComponent.getData().trim();

        registroConteudoFiltro.data = ConvertDateDBUtil.convertDateToDB(data);
        registroConteudoFiltro.horarios = horariosRectComponent.getHorarios();

        this.sendNotification(RegistroConteudoNotifications.READ_REGISTROS_CONTEUDOS, registroConteudoFiltro);

    }

    /** @override */
    public registerListeners(): void {

        this.addListener(RegistroConteudoNotifications.SUCCESS_READ_REGISTROS_CONTEUDOS, this.onSuccessReadRegistros);
        this.addListener(RegistroConteudoNotifications.FAILURE_READ_REGISTROS_CONTEUDOS, this.onFailureReadRegistros);

    }

    private onSuccessReadRegistros(registros: RegistroConteudo[]): void {

        this.registroConteudoPage.setLoading(false);

        let listaRegistrosConteudosComponent: ListaRegistrosConteudosComponent = this.registroConteudoPage.listaRegistrosConteudosComponent;
        listaRegistrosConteudosComponent.setRegistrosConteudos(registros);

    }

    private onFailureReadRegistros(response: Response): void {

        this.registroConteudoPage.setLoading(false);

        console.log(response);

    }

}
