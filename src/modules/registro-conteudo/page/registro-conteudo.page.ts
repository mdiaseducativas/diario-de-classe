import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ModalController, NavController} from "ionic-angular";

import {Page} from "../../../shared/core/base.page";
import {FrequenciaHorariosRectComponent} from "../../frequencia/components/horarios-rect/frequencia-horarios-rect.component";
import {HorariosRectContainerComponent} from "../../horarios/components/rect-container/horarios-rect-container.component";
import {EscolaRectContainerComponent} from "../../escola/components/rect-container/escola-rect-container.component";
import {RegistroConteudoEscolaMediator} from "./shared/registro-conteudo-escola.mediator";
import {RegistroConteudoHorariosMediator} from "./shared/registro-conteudo-horarios.mediator";
import {RegistroConteudoMediator} from "./shared/registro-conteudo.mediator";
import {ListaRegistrosConteudosComponent} from "../components/lista-registros-conteudos/lista-registros-conteudos.component";

@Component({
    selector: 'registro-conteudo-page',
    templateUrl: 'registro-conteudo.page.html',
    providers: []
})

export class RegistroConteudoPage extends Page implements OnInit, AfterViewInit {

    @ViewChild(EscolaRectContainerComponent)
    public escolaRectContainerComponent: EscolaRectContainerComponent;

    @ViewChild(HorariosRectContainerComponent)
    public horariosRectContainerComponent: HorariosRectContainerComponent;

    @ViewChild(ListaRegistrosConteudosComponent)
    public listaRegistrosConteudosComponent: ListaRegistrosConteudosComponent;

    public registroConteudoEscolaMediator: RegistroConteudoEscolaMediator;
    public registroConteudoHorariosMediator: RegistroConteudoHorariosMediator;
    public registroConteudoMediator: RegistroConteudoMediator;

    protected loading: boolean;

    constructor() {

        super();

        this.loading = false;

    }

    public ngOnInit(): void {

        this.registroConteudoEscolaMediator = new RegistroConteudoEscolaMediator(this);
        this.registroConteudoHorariosMediator = new RegistroConteudoHorariosMediator(this);
        this.registroConteudoMediator = new RegistroConteudoMediator(this);

        this.addMediator(this.registroConteudoEscolaMediator);
        this.addMediator(this.registroConteudoHorariosMediator);
        this.addMediator(this.registroConteudoMediator);

    }

    public ngAfterViewInit(): void {

        this.registroConteudoMediator.readRegistrosConteudos();

    }

    public setLoading(loading: boolean): void {

        this.loading = loading;

    }

}
