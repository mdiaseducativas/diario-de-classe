export class RegistroConteudoNotifications {

    public static READ_REGISTROS_CONTEUDOS: string = "RegistroConteudoNotifications.READ_REGISTROS_CONTEUDOS";
    public static SUCCESS_READ_REGISTROS_CONTEUDOS: string = "RegistroConteudoNotifications.SUCCESS_READ_REGISTROS_CONTEUDOS";
    public static FAILURE_READ_REGISTROS_CONTEUDOS: string = "RegistroConteudoNotifications.FAILURE_READ_REGISTROS_CONTEUDOS";

}
