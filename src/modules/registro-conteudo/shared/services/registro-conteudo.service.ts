import {Injectable} from '@angular/core';
import {Http} from "@angular/http";
import {Observable} from "rxjs/Rx";

import {Authentication} from "../../../../common/authentication/shared/services/authentication.service";
import {Service} from "../../../../shared/services/base.service";
import {ConfigService} from "../../../../common/config/shared/services/config.service";
import {UsuarioFiltro} from "../models/usuario-filtro.model";
import {RegistroConteudoFiltro} from "../models/registro-conteudo-filtro.model";

@Injectable()
export class RegistroConteudoService extends Service {

    private http: Http;
    private config: ConfigService;
    private authentication: Authentication;

    constructor(http: Http, config: ConfigService, authentication: Authentication) {

        super();

        this.http = http;
        this.config = config;
        this.authentication = authentication;

    }

    public getConteudos(registroConteudoFiltro: RegistroConteudoFiltro): Observable<Object> {

        let url: string = 'assets/data/registro-conteudo.retorno.json';
        // let url: string = this.config.getURL() + '/conteudos';

        return this.http.get(url, {
            headers: this.getHeaders()
        });

    }

}
