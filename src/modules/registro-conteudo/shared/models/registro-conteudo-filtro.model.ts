import {Horario} from "../../../horarios/shared/models/horario.model";

export class RegistroConteudoFiltro {

    data: string;
    horarios: Horario[];

    constructor() {
        this.horarios = [];
    }

}
