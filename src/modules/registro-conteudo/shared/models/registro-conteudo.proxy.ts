import IProxy = puremvc.IProxy;
import Proxy = puremvc.Proxy;
import {Response} from '@angular/http';
import {Observable} from "rxjs";
import 'rxjs/add/operator/map';

import {RegistroConteudoService} from "../services/registro-conteudo.service";
import {RegistroConteudoFactory} from "./registro-conteudo.factory";
import {RegistroConteudoFiltro} from "./registro-conteudo-filtro.model";

export class RegistroConteudoProxy extends Proxy implements IProxy {

    public static NAME: string = "RegistroConteudoProxy.NAME";

    public service: RegistroConteudoService;

    constructor(data?: any) {

        super(RegistroConteudoProxy.NAME, data);

    }

    public getRegistrosConteudos(registroConteudoFiltro: RegistroConteudoFiltro): Observable<Response> {

        let registroConteudoFactory: RegistroConteudoFactory = new RegistroConteudoFactory();
        let observer: Observable<Object> = this.service.getConteudos(registroConteudoFiltro);

        return observer
            .map((res: Response) => res.json())
            .map((data: Object) => registroConteudoFactory.create(data['data']));

    }

}
