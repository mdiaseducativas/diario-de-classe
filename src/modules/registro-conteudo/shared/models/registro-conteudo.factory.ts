import {Escola} from "./escola.model";
import {RegistroConteudo} from "./registro-conteudo.model";
import {ConvertDateDBUtil} from "../../../../shared/utils/convert-date-db.util";
import {Util} from "../../../../shared/utils/util";

export class RegistroConteudoFactory {

    public create(data: any): any {

        switch (data.constructor) {

            case Object: {
                return this.createOne(data);
            }

            case Array: {
                return this.createMany(data);
            }

        }

    }

    private createOne(data: Object): RegistroConteudo {

        let registro: RegistroConteudo = new RegistroConteudo(data);
        registro.data = ConvertDateDBUtil.convertDateFromDB(registro.data);
        registro.status = Util.convertToBoolean(registro.status);

        return registro;

    }

    private createMany(data: Object[]): RegistroConteudo[] {

        let registrosConteudos: RegistroConteudo[] = [];

        for (let i: number = 0; i < data.length; i++) {
            let registroConteudo: RegistroConteudo = this.createOne(data[i]);
            registrosConteudos.push(registroConteudo);
        }

        return registrosConteudos;

    }

}
