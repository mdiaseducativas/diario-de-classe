import {Model} from "../../../../shared/model/base.model";

export class RegistroConteudo extends Model {

    data: string;
    descricao: string;
    status: boolean;

    constructor(data: Object = {}) {
        super();
        this.set(data);
    }

}
