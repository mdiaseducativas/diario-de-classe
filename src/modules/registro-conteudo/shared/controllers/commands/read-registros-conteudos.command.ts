import SimpleCommand = puremvc.SimpleCommand;
import ICommand = puremvc.ICommand;
import INotification = puremvc.INotification;
import IFacade = puremvc.IFacade;
import {Observable} from "rxjs/Observable";

import {AppFacade} from "../../../../../shared/core/application-facade";
import {RegistroConteudoProxy} from "../../models/registro-conteudo.proxy";
import {RegistroConteudoFiltro} from "../../models/registro-conteudo-filtro.model";
import {RegistroConteudo} from "../../models/registro-conteudo.model";
import {RegistroConteudoNotifications} from "../../notifications/registro-conteudo.notifications";

export class ReadRegistrosConteudosCommand extends SimpleCommand implements ICommand {

    /** @override */
    public execute(notification: INotification): void {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
        let proxy: RegistroConteudoProxy = facade.retrieveProxy(RegistroConteudoProxy.NAME) as RegistroConteudoProxy;
        let registroConteudoFiltro: RegistroConteudoFiltro = notification.getBody();

        let observer: Observable<Object> = proxy.getRegistrosConteudos(registroConteudoFiltro);
        observer.subscribe(
            (registros: RegistroConteudo[]) => this.sendNotification(RegistroConteudoNotifications.SUCCESS_READ_REGISTROS_CONTEUDOS, registros),
            () => this.sendNotification(RegistroConteudoNotifications.FAILURE_READ_REGISTROS_CONTEUDOS)
        );

    }

}
