import {Component} from '@angular/core';

import {Escola} from "../../shared/models/escola.model";
import {Usuario} from "../../shared/models/usuario.model";
import {Conteudo} from "../../shared/models/conteudo.model";
import {RegistroConteudo} from "../../shared/models/registro-conteudo.model";

@Component({
    selector: 'lista-registros-conteudos-component',
    templateUrl: 'lista-registros-conteudos.component.html',
    providers: []
})

export class ListaRegistrosConteudosComponent {

    protected loading: boolean;
    protected registros: RegistroConteudo[];

    constructor() {

        this.registros = [];
        this.loading = false;

    }

    public setLoading(loading: boolean): void {

        this.loading = loading;

    }

    public setRegistrosConteudos(conteudos: RegistroConteudo[]): void {

        this.registros = conteudos;

    }

    public getRegistrosConteudos(): RegistroConteudo[] {

        return this.registros;

    }

}
