import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {SharedModule} from "../../components/shared.module";
import {ConfigService} from "../../common/config/shared/services/config.service";
import {Authentication} from "../../common/authentication/shared/services/authentication.service";
import {EscolaModule} from "../escola/escola.module";
import {RegistroConteudoPage} from "./page/registro-conteudo.page";
import {HorariosModule} from "../horarios/horarios.module";
import IFacade = puremvc.IFacade;
import {AppFacade} from "../../shared/core/application-facade";
import {RegistroConteudoService} from "./shared/services/registro-conteudo.service";
import {RegistroConteudoProxy} from "./shared/models/registro-conteudo.proxy";
import {RegistroConteudoNotifications} from "./shared/notifications/registro-conteudo.notifications";
import {ReadRegistrosConteudosCommand} from "./shared/controllers/commands/read-registros-conteudos.command";
import {ListaRegistrosConteudosComponent} from "./components/lista-registros-conteudos/lista-registros-conteudos.component";

@NgModule({
    declarations: [
        RegistroConteudoPage,
        ListaRegistrosConteudosComponent
    ],
    imports: [
        IonicModule.forRoot(RegistroConteudoPage),
        SharedModule,
        EscolaModule,
        HorariosModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        RegistroConteudoPage,
        ListaRegistrosConteudosComponent
    ],
    providers: [
        Authentication,
        ConfigService,
        RegistroConteudoService,
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        }
    ]
})

export class RegistroConteudoModule {

    constructor(registroConteudoService: RegistroConteudoService) {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
        let registroConteudoProxy: RegistroConteudoProxy = new RegistroConteudoProxy();
        registroConteudoProxy.service = registroConteudoService;

        /** Register Proxy */
        facade.registerProxy(registroConteudoProxy);

        /** Register Commands */
        facade.registerCommand(RegistroConteudoNotifications.READ_REGISTROS_CONTEUDOS, ReadRegistrosConteudosCommand);

    }

}
