import SimpleCommand = puremvc.SimpleCommand;
import ICommand = puremvc.ICommand;
import INotification = puremvc.INotification;
import {Storage} from "@ionic/storage";

import {IonicEssentials} from "../../../../../../shared/model/ionic-essentials/ionic-essentials.model";
import {AnoLetivoConstants} from "../../../constants/ano-letivo.constants";
import {AnoLetivoStorageNotifications} from "../../../notifications/ano-letivo-storage.notifications";

export class DeleteAnosLetivosStorageCommand extends SimpleCommand implements ICommand {

    /** @override */
    public execute(notification: INotification): void {

        let storage: Storage = IonicEssentials.storage;
        storage
            .remove(AnoLetivoConstants.STORAGE_ANOS_LETIVOS)
            .then(() => this.sendNotification(AnoLetivoStorageNotifications.SUCCESS_DELETE_ANOS_LETIVOS))
            .catch(() => this.sendNotification(AnoLetivoStorageNotifications.FAILURE_DELETE_ANOS_LETIVOS));

    }

}
