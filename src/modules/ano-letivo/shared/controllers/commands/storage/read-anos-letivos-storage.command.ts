import SimpleCommand = puremvc.SimpleCommand;
import ICommand = puremvc.ICommand;
import INotification = puremvc.INotification;
import {Storage} from "@ionic/storage";

import {IonicEssentials} from "../../../../../../shared/model/ionic-essentials/ionic-essentials.model";
import {AnoLetivo} from "../../../models/ano-letivo.model";
import {AnoLetivoStorageNotifications} from "../../../notifications/ano-letivo-storage.notifications";
import {AnoLetivoConstants} from "../../../constants/ano-letivo.constants";
import {AnoLetivoFactory} from "../../../models/ano-letivo.factory";

export class ReadAnosLetivosStorageCommand extends SimpleCommand implements ICommand {

    /** @override */
    public execute(notification: INotification): void {

        let storage: Storage = IonicEssentials.storage;
        storage
            .ready()
            .then(() => this.onStorageReady(storage))
            .catch(() => this.onFailure());

    }

    private onStorageReady(storage: Storage): void {

        storage
            .get(AnoLetivoConstants.STORAGE_ANOS_LETIVOS)
            .then((data: string) => this.onDataReady(data))
            .catch(() => this.onFailure());

    }

    private onDataReady(data: string): void {

        let dataStorage: Object[] = JSON.parse(data || "[]");
        let factory: AnoLetivoFactory = new AnoLetivoFactory();
        let anosLetivos: AnoLetivo[] = factory.create(dataStorage);

        this.sendNotification(AnoLetivoStorageNotifications.SUCCESS_READ_ANOS_LETIVOS, anosLetivos);

    }

    private onFailure(): void {

        this.sendNotification(AnoLetivoStorageNotifications.FAILURE_READ_ANOS_LETIVOS);

    }

}
