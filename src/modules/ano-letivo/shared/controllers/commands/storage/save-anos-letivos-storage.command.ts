import SimpleCommand = puremvc.SimpleCommand;
import ICommand = puremvc.ICommand;
import INotification = puremvc.INotification;
import {Storage} from "@ionic/storage";

import {IonicEssentials} from "../../../../../../shared/model/ionic-essentials/ionic-essentials.model";
import {AnoLetivoStorageNotifications} from "../../../notifications/ano-letivo-storage.notifications";
import {AnoLetivo} from "../../../models/ano-letivo.model";
import {AnoLetivoConstants} from "../../../constants/ano-letivo.constants";

export class SaveAnosLetivosStorageCommand extends SimpleCommand implements ICommand {

    /** @override */
    public execute(notification: INotification): void {

        let anos: AnoLetivo[] = notification.getBody();
        let storage: Storage = IonicEssentials.storage;
        storage
            .ready()
            .then(() => this.onStorageReady(storage, anos))
            .catch(() => this.sendNotification(AnoLetivoStorageNotifications.FAILURE_SAVE_ANOS_LETIVOS));

    }

    private onStorageReady(storage: Storage, anos: AnoLetivo[]): void {

        let data: string = JSON.stringify(anos);
        storage
            .set(AnoLetivoConstants.STORAGE_ANOS_LETIVOS, data)
            .then(() => this.sendNotification(AnoLetivoStorageNotifications.SUCCESS_SAVE_ANOS_LETIVOS))
            .catch(() => this.sendNotification(AnoLetivoStorageNotifications.FAILURE_SAVE_ANOS_LETIVOS));

    }

}
