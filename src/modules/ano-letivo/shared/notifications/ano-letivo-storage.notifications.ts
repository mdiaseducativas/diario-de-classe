export class AnoLetivoStorageNotifications {

    public static SAVE_ANOS_LETIVOS: string = "AnoLetivoStorageNotifications.SAVE_ANOS_LETIVOS";
    public static SUCCESS_SAVE_ANOS_LETIVOS: string = "AnoLetivoStorageNotifications.SUCCESS_SAVE_ANOS_LETIVOS";
    public static FAILURE_SAVE_ANOS_LETIVOS: string = "AnoLetivoStorageNotifications.FAILURE_SAVE_ANOS_LETIVOS";

    public static READ_ANOS_LETIVOS: string = "AnoLetivoStorageNotifications.READ_ANOS_LETIVOS";
    public static SUCCESS_READ_ANOS_LETIVOS: string = "AnoLetivoStorageNotifications.SUCCESS_READ_ANOS_LETIVOS";
    public static FAILURE_READ_ANOS_LETIVOS: string = "AnoLetivoStorageNotifications.FAILURE_READ_ANOS_LETIVOS";

    public static DELETE_ANOS_LETIVOS: string = "AnoLetivoStorageNotifications.DELETE_ANOS_LETIVOS";
    public static SUCCESS_DELETE_ANOS_LETIVOS: string = "AnoLetivoStorageNotifications.SUCCESS_DELETE_ANOS_LETIVOS";
    public static FAILURE_DELETE_ANOS_LETIVOS: string = "AnoLetivoStorageNotifications.FAILURE_DELETE_ANOS_LETIVOS";

}
