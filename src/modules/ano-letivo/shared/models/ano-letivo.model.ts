import {Model} from "../../../../shared/model/base.model";

export class AnoLetivo extends Model {

    id_ano_letivo: string;
    nome: string;

    constructor(data: Object = {}) {
        super();
        this.set(data);
    }

}
