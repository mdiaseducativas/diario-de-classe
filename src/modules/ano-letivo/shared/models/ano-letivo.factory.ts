import {Escola} from "./escola.model";
import {AnoLetivo} from "./ano-letivo.model";

export class AnoLetivoFactory {

    public create(data: any): any {

        switch (data.constructor) {

            case Object: {
                return this.createOne(data);
            }

            case Array: {
                return this.createMany(data);
            }

        }

    }

    private createOne(data: Object): AnoLetivo {

        let anoLetivo: AnoLetivo = new AnoLetivo(data);
        return anoLetivo;

    }

    private createMany(data: Object[]): AnoLetivo[] {

        let anos: AnoLetivo[] = [];

        for (let i: number = 0; i < data.length; i++) {
            let anoLetivo: AnoLetivo = this.createOne(data[i]);
            anos.push(anoLetivo);
        }

        return anos;

    }

}
