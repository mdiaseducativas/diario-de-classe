import {Component, EventEmitter, Output} from '@angular/core';
import {AnoLetivo} from "../../shared/models/ano-letivo.model";

@Component({
    selector: 'select-ano-letivo-component',
    templateUrl: 'select-ano-letivo.component.html',
    providers: []
})

export class SelectAnoLetivoComponent {

    @Output() onSelectAnoLetivo: EventEmitter<AnoLetivo>;

    protected anoLetivo: AnoLetivo;
    protected anosLetivos: AnoLetivo[];

    constructor() {

        this.anosLetivos = [];
        this.anoLetivo = new AnoLetivo();
        this.onSelectAnoLetivo = new EventEmitter<AnoLetivo>();

    }

    public setAnoLetivo(anoLetivo: AnoLetivo): void {

        this.anoLetivo = anoLetivo;

    }

    public setAnosLetivos(anosLetivos: AnoLetivo[]): void {

        this.anosLetivos = anosLetivos;

    }

    protected selectAnoLetivo(anoLetivo: AnoLetivo): void {

        this.setAnoLetivo(anoLetivo);

        this.onSelectAnoLetivo.emit(anoLetivo);

    }

}
