import IFacade = puremvc.IFacade;
import {NgModule} from '@angular/core';
import {IonicApp} from 'ionic-angular';

import {AppFacade} from "../../shared/core/application-facade";
import {AnoLetivoStorageNotifications} from "./shared/notifications/ano-letivo-storage.notifications";
import {ReadAnosLetivosStorageCommand} from "./shared/controllers/commands/storage/read-anos-letivos-storage.command";
import {SaveAnosLetivosStorageCommand} from "./shared/controllers/commands/storage/save-anos-letivos-storage.command";
import {DeleteAnosLetivosStorageCommand} from "./shared/controllers/commands/storage/delete-anos-letivos-storage.command";
import {SelectAnoLetivoComponent} from "./components/select-ano-letivo/select-ano-letivo.component";

@NgModule({
    imports: [],
    declarations: [SelectAnoLetivoComponent],
    entryComponents: [SelectAnoLetivoComponent],
    exports: [SelectAnoLetivoComponent],
    bootstrap: [IonicApp],
    providers: []
})

export class AnoLetivoModule {

    constructor() {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);

        /** Register Commands */
        facade.registerCommand(AnoLetivoStorageNotifications.READ_ANOS_LETIVOS, ReadAnosLetivosStorageCommand);
        facade.registerCommand(AnoLetivoStorageNotifications.SAVE_ANOS_LETIVOS, SaveAnosLetivosStorageCommand);
        facade.registerCommand(AnoLetivoStorageNotifications.DELETE_ANOS_LETIVOS, DeleteAnosLetivosStorageCommand);

    }

}
