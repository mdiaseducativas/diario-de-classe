import IFacade = puremvc.IFacade;
import {NgModule} from '@angular/core';
import {IonicApp} from 'ionic-angular';
import {BrowserModule} from "@angular/platform-browser";

import {AppFacade} from "../../shared/core/application-facade";
import {UsuarioService} from "./shared/services/usuario.service";
import {UsuarioStorageNotifications} from "./shared/notifications/usuario-storage.notifications";
import {UsuarioNotifications} from "./shared/notifications/usuario.notifications";
import {ReadUsuarioStorageCommand} from "./shared/controllers/commands/storage/read-usuario-storage.command";
import {SaveUsuarioStorageCommand} from "./shared/controllers/commands/storage/save-usuario-storage.command";
import {DeleteUsuarioStorageCommand} from "./shared/controllers/commands/storage/delete-usuario-storage.command";
import {ReadMeCommand} from "./shared/controllers/commands/me/read-me.command";
import {UsuarioProxy} from "./shared/models/usuario.proxy";
import {SaveMeCommand} from "./shared/controllers/commands/me/save-me.command";
import {UsuarioGridComponent} from "./components/grid/usuario-grid.component";
import {ReadUsuariosCommand} from "./shared/controllers/commands/usuarios/read-usuarios-command";
import {ReadFrequenciaUsuariosCommand} from "./shared/controllers/commands/usuarios/read-frequencia-usuarios.command";
import {ChangeFrequenciaUsuarioCommand} from "./shared/controllers/commands/usuarios/change-frequencia-usuario.command";
import {UsuarioListNotasComponent} from "./components/list-notas/usuario-list-notas.component";
import {ReadLancamentoNotasUsuarioCommand} from "./shared/controllers/commands/usuarios/read-lancamento-notas-usuario.command";
import {UsuarioUnidadesComponent} from "./components/unidades/usuario-unidades.component";


@NgModule({
    declarations: [
        UsuarioGridComponent,
        UsuarioListNotasComponent,
        UsuarioUnidadesComponent
    ],
    entryComponents: [
        UsuarioGridComponent,
        UsuarioListNotasComponent,
        UsuarioUnidadesComponent
    ],
    exports: [
        UsuarioGridComponent,
        UsuarioListNotasComponent,
        UsuarioUnidadesComponent
    ],
    imports: [BrowserModule],
    bootstrap: [IonicApp],
    providers: [UsuarioService]
})

export class UsuarioModule {

    constructor(usuarioService: UsuarioService) {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
        let usuarioProxy: UsuarioProxy = new UsuarioProxy();
        usuarioProxy.service = usuarioService;

        /** Register Proxy */
        facade.registerProxy(usuarioProxy);

        /** Register Commands */
        facade.registerCommand(UsuarioNotifications.READ_ME, ReadMeCommand);
        facade.registerCommand(UsuarioNotifications.SAVE_ME, SaveMeCommand);
        facade.registerCommand(UsuarioNotifications.READ_USUARIOS, ReadUsuariosCommand);
        facade.registerCommand(UsuarioNotifications.READ_LANCAMENTO_NOTAS_USUARIO, ReadLancamentoNotasUsuarioCommand);
        facade.registerCommand(UsuarioNotifications.READ_FREQUENCIA_USUARIOS, ReadFrequenciaUsuariosCommand);
        facade.registerCommand(UsuarioNotifications.CHANGE_FREQUENCIA_USUARIO, ChangeFrequenciaUsuarioCommand);

        facade.registerCommand(UsuarioStorageNotifications.READ_USUARIO, ReadUsuarioStorageCommand);
        facade.registerCommand(UsuarioStorageNotifications.SAVE_USUARIO, SaveUsuarioStorageCommand);
        facade.registerCommand(UsuarioStorageNotifications.DELETE_USUARIO, DeleteUsuarioStorageCommand);

    }

}
