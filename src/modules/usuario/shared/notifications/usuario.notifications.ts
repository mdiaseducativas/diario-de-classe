export class UsuarioNotifications {

    public static READ_ME: string = "UsuarioNotifications.READ_ME";
    public static SUCCESS_READ_ME: string = "UsuarioNotifications.SUCCESS_READ_ME";
    public static FAILURE_READ_ME: string = "UsuarioNotifications.FAILURE_READ_ME";

    public static SAVE_ME: string = "UsuarioNotifications.SAVE_ME";
    public static SUCCESS_SAVE_ME: string = "UsuarioNotifications.SUCCESS_SAVE_ME";
    public static FAILURE_SAVE_ME: string = "UsuarioNotifications.FAILURE_SAVE_ME";

    public static UPDATE_ME: string = "UsuarioNotifications.UPDATE_ME";
    public static SUCCESS_UPDATE_ME: string = "UsuarioNotifications.SUCCESS_UPDATE_ME";
    public static FAILURE_UPDATE_ME: string = "UsuarioNotifications.FAILURE_UPDATE_ME";

    public static READ_USUARIOS: string = "UsuarioNotifications.READ_USUARIOS";
    public static SUCCESS_READ_USUARIOS: string = "UsuarioNotifications.SUCCESS_READ_USUARIOS";
    public static FAILURE_READ_USUARIOS: string = "UsuarioNotifications.FAILURE_READ_USUARIOS";

    public static READ_FREQUENCIA_USUARIOS: string = "UsuarioNotifications.READ_FREQUENCIA_USUARIOS";
    public static SUCCESS_READ_FREQUENCIA_USUARIOS: string = "UsuarioNotifications.SUCCESS_READ_FREQUENCIA_USUARIOS";
    public static FAILURE_READ_FREQUENCIA_USUARIOS: string = "UsuarioNotifications.FAILURE_READ_FREQUENCIA_USUARIOS";

    public static CHANGE_FREQUENCIA_USUARIO: string = "UsuarioNotifications.CHANGE_FREQUENCIA_USUARIO";
    public static SUCCESS_CHANGE_FREQUENCIA_USUARIO: string = "UsuarioNotifications.SUCCESS_CHANGE_FREQUENCIA_USUARIO";
    public static FAILURE_CHANGE_FREQUENCIA_USUARIO: string = "UsuarioNotifications.FAILURE_CHANGE_FREQUENCIA_USUARIO";

    public static READ_LANCAMENTO_NOTAS_USUARIO: string = "UsuarioNotifications.READ_LANCAMENTO_NOTAS_USUARIO";
    public static SUCCESS_READ_LANCAMENTO_NOTAS_USUARIO: string = "UsuarioNotifications.SUCCESS_READ_LANCAMENTO_NOTAS_USUARIO";
    public static FAILURE_READ_LANCAMENTO_NOTAS_USUARIO: string = "UsuarioNotifications.FAILURE_READ_LANCAMENTO_NOTAS_USUARIO";

}
