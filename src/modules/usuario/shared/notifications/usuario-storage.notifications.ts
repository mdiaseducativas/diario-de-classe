export class UsuarioStorageNotifications {

    public static SAVE_USUARIO: string = "UsuarioStorageNotifications.SAVE_USUARIO";
    public static SUCCESS_SAVE_USUARIO: string = "UsuarioStorageNotifications.SUCCESS_SAVE_USUARIO";
    public static FAILURE_SAVE_USUARIO: string = "UsuarioStorageNotifications.FAILURE_SAVE_USUARIO";

    public static READ_USUARIO: string = "UsuarioStorageNotifications.READ_USUARIO";
    public static SUCCESS_READ_USUARIO: string = "UsuarioStorageNotifications.SUCCESS_READ_USUARIO";
    public static FAILURE_READ_USUARIO: string = "UsuarioStorageNotifications.FAILURE_READ_USUARIO";

    public static DELETE_USUARIO: string = "UsuarioStorageNotifications.DELETE_USUARIO";
    public static SUCCESS_DELETE_USUARIO: string = "UsuarioStorageNotifications.SUCCESS_DELETE_USUARIO";
    public static FAILURE_DELETE_USUARIO: string = "UsuarioStorageNotifications.FAILURE_DELETE_USUARIO";

}
