import {Injectable} from '@angular/core';
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Rx";

import {Usuario} from "../models/usuario.model";
import {Authentication} from "../../../../common/authentication/shared/services/authentication.service";
import {Service} from "../../../../shared/services/base.service";
import {ConfigService} from "../../../../common/config/shared/services/config.service";
import {UsuarioFiltro} from "../models/usuario-filtro.model";

@Injectable()
export class UsuarioService extends Service {

    private http: Http;
    private config: ConfigService;
    private authentication: Authentication;

    constructor(http: Http, config: ConfigService, authentication: Authentication) {

        super();

        this.http = http;
        this.config = config;
        this.authentication = authentication;

    }

    public getMe(): Observable<Object> {

        let url: string = this.config.getURL() + '/me';

        return this.http.get(url, {
            headers: this.getHeaders()
        });

    }

    public changeFrequenciaUsuario(usuario: Usuario): Observable<Response> {

        // let body: string = JSON.stringify({
        //     frequencia: usuario.frequencia,
        //     id_usuario: usuario.id_usuario
        // });
        //
        // let url: string = this.config.getURL() + '/usuarios/frequencia';
        // return this.http.post(url, body, {
        //     headers: this.getHeaders()
        // });

        let url: string = 'assets/data/usuarios.retorno.json';
        return this.http.get(url, {
            headers: this.getHeaders()
        });
    }

    public getUsuarios(usuarioFiltro: UsuarioFiltro): Observable<Response> {

        // let url: string = this.config.getURL() + '/usuarios';
        // let params: URLSearchParams = new URLSearchParams();

        let url: string = 'assets/data/usuarios.retorno.json';
        return this.http.get(url, {
            headers: this.getHeaders()
        });

    }

    public getFrequenciaUsuarios(usuarioFiltro: UsuarioFiltro): Observable<Response> {

        // let url: string = this.config.getURL() + '/usuarios';
        // let params: URLSearchParams = new URLSearchParams();

        let url: string = 'assets/data/usuarios-frequencia.retorno.json';

        return this.http.get(url, {
            headers: this.getHeaders()
        });

    }

    public getLancamentoNotasUsuario(usuarioFiltro: UsuarioFiltro): Observable<Response> {

        // let url: string = this.config.getURL() + '/usuarios';
        // let params: URLSearchParams = new URLSearchParams();

        let url: string = 'assets/data/usuario-lancamento-notas.retorno.json';

        return this.http.get(url, {
            headers: this.getHeaders()
        });

    }

    public saveMe(usuario: Usuario): Observable<Response> {

        let url: string = this.config.getURL() + '/me';
        let data: Object = {
            data: usuario
        };

        let payload: string = JSON.stringify(data);
        return this.http.put(url, payload, {
            headers: this.getHeaders()
        });

    }

}
