import SimpleCommand = puremvc.SimpleCommand;
import ICommand = puremvc.ICommand;
import INotification = puremvc.INotification;
import {Storage} from "@ionic/storage";

import {Usuario} from "../../../models/usuario.model";
import {IonicEssentials} from "../../../../../../shared/model/ionic-essentials/ionic-essentials.model";
import {UsuarioConstants} from "../../../constans/usuario.constants";
import {UsuarioStorageNotifications} from "../../../notifications/usuario-storage.notifications";

export class ReadUsuarioStorageCommand extends SimpleCommand implements ICommand {

    /** @override */
    public execute(notification: INotification): void {

        let storage: Storage = IonicEssentials.storage;
        storage
            .ready()
            .then(() => this.onStorageReady(storage))
            .catch(() => this.onFailure());

    }

    private onStorageReady(storage: Storage): void {

        storage
            .get(UsuarioConstants.STORAGE)
            .then((data: string) => this.onDataReady(data))
            .catch(() => this.onFailure());

    }

    private onDataReady(data): void {

        let dataStorage: Object[] = JSON.parse(data || "{}");
        let usuario: Usuario = new Usuario(dataStorage);

        this.sendNotification(UsuarioStorageNotifications.SUCCESS_READ_USUARIO, usuario);

    }

    private onFailure(): void {

        this.sendNotification(UsuarioStorageNotifications.FAILURE_READ_USUARIO);

    }

}
