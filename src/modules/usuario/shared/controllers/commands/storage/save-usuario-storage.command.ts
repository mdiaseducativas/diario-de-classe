import SimpleCommand = puremvc.SimpleCommand;
import ICommand = puremvc.ICommand;
import INotification = puremvc.INotification;
import {Storage} from "@ionic/storage";

import {IonicEssentials} from "../../../../../../shared/model/ionic-essentials/ionic-essentials.model";
import {Usuario} from "../../../models/usuario.model";
import {UsuarioConstants} from "../../../constans/usuario.constants";
import {UsuarioStorageNotifications} from "../../../notifications/usuario-storage.notifications";

export class SaveUsuarioStorageCommand extends SimpleCommand implements ICommand {

    /** @override */
    public execute(notification: INotification): void {

        let usuario: Usuario = notification.getBody();
        let storage: Storage = IonicEssentials.storage;
        storage
            .ready()
            .then(() => this.onStorageReady(storage, usuario))
            .catch(() => this.sendNotification(UsuarioStorageNotifications.FAILURE_SAVE_USUARIO));

    }

    private onStorageReady(storage: Storage, usuario: Usuario): void {

        let data: string = JSON.stringify(usuario);
        storage
            .set(UsuarioConstants.STORAGE, data)
            .then(() => this.sendNotification(UsuarioStorageNotifications.SUCCESS_SAVE_USUARIO));

    }

}
