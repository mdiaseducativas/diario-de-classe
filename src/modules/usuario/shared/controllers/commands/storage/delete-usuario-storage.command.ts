import SimpleCommand = puremvc.SimpleCommand;
import ICommand = puremvc.ICommand;
import INotification = puremvc.INotification;
import {Storage} from "@ionic/storage";

import {IonicEssentials} from "../../../../../../shared/model/ionic-essentials/ionic-essentials.model";
import {UsuarioConstants} from "../../../constans/usuario.constants";
import {UsuarioStorageNotifications} from "../../../notifications/usuario-storage.notifications";

export class DeleteUsuarioStorageCommand extends SimpleCommand implements ICommand {

    /** @override */
    public execute(notification: INotification): void {

        let storage: Storage = IonicEssentials.storage;
        storage
            .remove(UsuarioConstants.STORAGE)
            .then(() => this.sendNotification(UsuarioStorageNotifications.SUCCESS_DELETE_USUARIO))
            .catch(() => this.sendNotification(UsuarioStorageNotifications.FAILURE_DELETE_USUARIO));

    }

}
