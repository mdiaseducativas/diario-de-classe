import SimpleCommand = puremvc.SimpleCommand;
import ICommand = puremvc.ICommand;
import INotification = puremvc.INotification;
import IFacade = puremvc.IFacade;
import {Observable} from "rxjs/Observable";

import {AppFacade} from "../../../../../../shared/core/application-facade";
import {UsuarioProxy} from "../../../models/usuario.proxy";
import {Usuario} from "../../../models/usuario.model";
import {UsuarioNotifications} from "../../../notifications/usuario.notifications";

export class SaveMeCommand extends SimpleCommand implements ICommand {

    /** @override */
    public execute(notification: INotification): void {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
        let proxy: UsuarioProxy = facade.retrieveProxy(UsuarioProxy.NAME) as UsuarioProxy;

        let usuario: Usuario = notification.getBody();
        let observer: Observable<Object> = proxy.saveMe(usuario);
        observer.subscribe(
            () => this.sendNotification(UsuarioNotifications.SUCCESS_SAVE_ME, usuario),
            () => this.sendNotification(UsuarioNotifications.FAILURE_SAVE_ME)
        );

    }

}
