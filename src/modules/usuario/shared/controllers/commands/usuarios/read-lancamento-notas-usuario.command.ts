import SimpleCommand = puremvc.SimpleCommand;
import ICommand = puremvc.ICommand;
import INotification = puremvc.INotification;
import IFacade = puremvc.IFacade;
import {Observable} from "rxjs/Observable";

import {AppFacade} from "../../../../../../shared/core/application-facade";
import {UsuarioProxy} from "../../../models/usuario.proxy";
import {UsuarioFiltro} from "../../../models/usuario-filtro.model";
import {Usuario} from "../../../models/usuario.model";
import {UsuarioNotifications} from "../../../notifications/usuario.notifications";

export class ReadLancamentoNotasUsuarioCommand extends SimpleCommand implements ICommand {

    /** @override */
    public execute(notification: INotification): void {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
        let proxy: UsuarioProxy = facade.retrieveProxy(UsuarioProxy.NAME) as UsuarioProxy;
        let usuarioFiltro: UsuarioFiltro = notification.getBody();

        let observer: Observable<Object> = proxy.getLancamentoNotasUsuario(usuarioFiltro);
        observer.subscribe(
            (usuario: Usuario) => this.sendNotification(UsuarioNotifications.SUCCESS_READ_LANCAMENTO_NOTAS_USUARIO, usuario),
            () => this.sendNotification(UsuarioNotifications.FAILURE_READ_LANCAMENTO_NOTAS_USUARIO)
        );

    }

}
