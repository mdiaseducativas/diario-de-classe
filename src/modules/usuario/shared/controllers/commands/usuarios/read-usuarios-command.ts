import SimpleCommand = puremvc.SimpleCommand;
import ICommand = puremvc.ICommand;
import INotification = puremvc.INotification;
import IFacade = puremvc.IFacade;
import {Observable} from "rxjs/Observable";

import {AppFacade} from "../../../../../../shared/core/application-facade";
import {UsuarioProxy} from "../../../models/usuario.proxy";
import {Usuario} from "../../../models/usuario.model";
import {UsuarioNotifications} from "../../../notifications/usuario.notifications";
import {UsuarioFiltro} from "../../../models/usuario-filtro.model";

export class ReadUsuariosCommand extends SimpleCommand implements ICommand {

    /** @override */
    public execute(notification: INotification): void {

        let facade: IFacade = AppFacade.getInstance(AppFacade.APP_STARTUP);
        let proxy: UsuarioProxy = facade.retrieveProxy(UsuarioProxy.NAME) as UsuarioProxy;
        let usuarioFiltro: UsuarioFiltro = notification.getBody();

        let observer: Observable<Object> = proxy.getUsuarios(usuarioFiltro);
        observer.subscribe(
            (usuarios: Usuario[]) => this.sendNotification(UsuarioNotifications.SUCCESS_READ_USUARIOS, usuarios),
            () => this.sendNotification(UsuarioNotifications.FAILURE_READ_USUARIOS)
        );

    }

}
