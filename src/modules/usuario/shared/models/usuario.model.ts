import {Turma} from '../../../turma/shared/models/turma.model';
import {Model} from "../../../../shared/model/base.model";
import {Perfil} from "../../../../common/perfil/shared/models/perfil.model";
import {Escola} from "../../../escola/shared/models/escola.model";
import {FrequenciaConstants} from "../../../../shared/constants/frequencia.constants";
import {Unidade} from "../../../unidade/shared/models/unidade.model";

export class Usuario extends Model {

    id_usuario: string;
    id_auth: string;
    nome: string;
    login: string;
    usuario: string;
    email: string;
    foto: string;
    status: string;
    senha: string;
    confimar_senha: string;
    matricula: string;
    cpf: string;
    push_id: string;
    versao: string;
    frequencia: string;
    ativo: boolean;
    plataforma: number;
    media_geral: string;

    perfil: Perfil;
    escola: Escola;
    turmas: Turma[];
    responsaveis: Usuario[];
    unidades: Unidade[];

    constructor(data: Object = {}) {
        super();
        this.set(data);
    }

    public static mudarFrequencia(frequencia: string) {

        switch (frequencia) {
            case FrequenciaConstants.PRESENCA:
                return FrequenciaConstants.FALTA_JUSTIFICADA;
            case FrequenciaConstants.FALTA_JUSTIFICADA:
                return FrequenciaConstants.FALTA;
            case FrequenciaConstants.FALTA:
                return FrequenciaConstants.PRESENCA;
        }

    }

}
