import {Usuario} from '../../../../shared/model';
import {Horario} from "../../../horarios/shared/models/horario.model";
import {EscolaFiltro} from "../../../escola/shared/models/escola-filtro.model";

export class UsuarioFiltro {

    usuario: Usuario;
    data: string;
    horarios: Horario[];
    escolaFiltro: EscolaFiltro;

}
