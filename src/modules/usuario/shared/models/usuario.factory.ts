import {Escola} from "./escola.model";
import {Usuario} from "./usuario.model";

export class UsuarioFactory {

    public create(data: any): any {

        switch (data.constructor) {

            case Object: {
                return this.createOne(data);
            }

            case Array: {
                return this.createMany(data);
            }

        }

    }

    private createOne(data: Object): Usuario {

        let usuario: Usuario = new Usuario(data);
        return usuario;

    }

    private createMany(data: Object[]): Usuario[] {

        let usuarios: Usuario[] = [];

        for (let i: number = 0; i < data.length; i++) {
            let usuario: Usuario = this.createOne(data[i]);
            usuarios.push(usuario);
        }

        return usuarios;

    }

}
