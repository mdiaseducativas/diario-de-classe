import IProxy = puremvc.IProxy;
import Proxy = puremvc.Proxy;
import {Response} from '@angular/http';
import {Observable} from "rxjs";
import 'rxjs/add/operator/map';

import {UsuarioService} from "../services/usuario.service";
import {Usuario} from "./usuario.model";
import {UsuarioFactory} from "./usuario.factory";
import {UsuarioFiltro} from "./usuario-filtro.model";

export class UsuarioProxy extends Proxy implements IProxy {

    public static NAME: string = "UsuarioProxy.NAME";

    public service: UsuarioService;

    constructor(data?: any) {

        super(UsuarioProxy.NAME, data);

    }

    public getMe(): Observable<Response> {

        let usuarioFactory: UsuarioFactory = new UsuarioFactory();
        let observer: Observable<Object> = this.service.getMe();

        return observer
            .map((res: Response) => res.json())
            .map((data: Object) => usuarioFactory.create(data['data']));

    }

    public getUsuarios(usuarioFiltro: UsuarioFiltro): Observable<Response> {

        let usuarioFactory: UsuarioFactory = new UsuarioFactory();
        let observer: Observable<Object> = this.service.getUsuarios(usuarioFiltro);

        return observer
            .map((res: Response) => res.json())
            .map((data: Object) => usuarioFactory.create(data['data']));

    }

    public getFrequenciaUsuarios(usuarioFiltro: UsuarioFiltro): Observable<Response> {

        let usuarioFactory: UsuarioFactory = new UsuarioFactory();
        let observer: Observable<Object> = this.service.getFrequenciaUsuarios(usuarioFiltro);

        return observer
            .map((res: Response) => res.json())
            .map((data: Object) => usuarioFactory.create(data['data']));

    }

    public getLancamentoNotasUsuario(usuarioFiltro: UsuarioFiltro): Observable<Response> {

        let usuarioFactory: UsuarioFactory = new UsuarioFactory();
        let observer: Observable<Object> = this.service.getLancamentoNotasUsuario(usuarioFiltro);

        return observer
            .map((res: Response) => res.json())
            .map((data: Object) => usuarioFactory.create(data['data']));

    }

    public changeFrequenciaUsuario(usuario: Usuario): Observable<Response> {

        let usuarioFactory: UsuarioFactory = new UsuarioFactory();
        let observer: Observable<Object> = this.service.changeFrequenciaUsuario(usuario);

        return observer
            .map((res: Response) => res.json())
            .map((data: Object) => usuarioFactory.create(data['data']));

    }

    public saveMe(usuario: Usuario): Observable<Response> {

        let observer: Observable<Object> = this.service.saveMe(usuario);
        return observer.map((res: Response) => res.json());

    }

}
