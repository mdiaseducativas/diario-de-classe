import {Component, EventEmitter, Input, Output} from '@angular/core';

import {Escola} from "../../shared/models/escola.model";
import {Usuario} from "../../shared/models/usuario.model";
import {FrequenciaConstants} from "../../../../shared/constants/frequencia.constants";

@Component({
    selector: 'usuario-grid-component',
    templateUrl: 'usuario-grid.component.html',
    providers: []
})

export class UsuarioGridComponent {

    @Output()
    public onSelectUsuario: EventEmitter<Usuario>;

    @Input()
    public check: boolean;

    protected loading: boolean;
    protected usuario: Usuario;
    protected usuarios: Usuario[];
    protected FREQUENCIA: FrequenciaConstants;

    constructor() {

        this.usuarios = [];
        this.usuario = null;
        this.loading = false;
        this.check = true;

        this.FREQUENCIA = FrequenciaConstants;

        this.onSelectUsuario = new EventEmitter<Usuario>();

    }

    public setLoading(loading: boolean): void {

        this.loading = loading;

    }

    public setUsuarios(usuarios: Usuario[]): void {

        usuarios.forEach((usuario: Usuario) => {
            usuario.nome = this.getCutName(usuario);
        });

        this.usuarios = usuarios;

    }

    public setUsuario(usuario: Usuario): void {

        this.usuario = usuario;

    }

    protected selectUsuario(usuario: Usuario): void {

        this.setUsuario(usuario);

        this.onSelectUsuario.emit(usuario);

    }

    private getCutName(usuario: Usuario): string {

        let nome: string = usuario.nome;
        let maxCaracteres: number = 12;

        if (usuario.nome && usuario.nome.length > maxCaracteres) {
            nome = '';
            for (let i: number = 0; i < maxCaracteres; i++) {
                nome += usuario.nome[i];
            }
            nome += '...';
        }

        return nome;

    }

}
