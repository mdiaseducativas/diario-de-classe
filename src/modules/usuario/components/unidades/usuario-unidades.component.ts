import {Component, Input} from '@angular/core';

import {Escola} from "../../shared/models/escola.model";
import {Usuario} from "../../shared/models/usuario.model";
import {Prova} from "../../../../shared/model/prova/prova.model";

@Component({
    selector: 'usuario-unidades-component',
    templateUrl: 'usuario-unidades.component.html',
    providers: []
})

export class UsuarioUnidadesComponent {

    @Input()
    protected usuario: Usuario;

    protected prova: Prova;
    protected loading: boolean;
    protected selectMode: boolean;
    protected notas: string[];

    constructor() {

        this.usuario = null;
        this.prova = null;
        this.selectMode = false;
        this.loading = false;
        this.notas = [];

        this.gerarNotas();

    }

    private gerarNotas(): void {

        for (let i: number = 0; i <= 10;) {
            this.notas.push(String(i));
            i += 0.5;
        }

    }

    public setLoading(loading: boolean): void {

        this.loading = loading;

    }

    public setUsuario(usuario: Usuario): void {

        this.usuario = usuario;

    }

    protected escolherNota(prova: Prova): void {

        this.prova = prova;
        this.selectMode = true;

    }

    protected selectNota(nota: string): void {

        this.prova.nota = nota;
        this.selectMode = false;

    }

}
