import {Component, EventEmitter, Output} from '@angular/core';

import {Escola} from "../../shared/models/escola.model";
import {Usuario} from "../../shared/models/usuario.model";

@Component({
    selector: 'usuario-list-notas-component',
    templateUrl: 'usuario-list-notas.component.html',
    providers: []
})

export class UsuarioListNotasComponent {

    @Output() public onChangeNota: EventEmitter<Usuario>;

    protected loading: boolean;
    protected usuario: Usuario;
    protected usuarios: Usuario[];

    constructor() {

        this.usuarios = [];
        this.usuario = null;
        this.loading = false;

    }

    public setLoading(loading: boolean): void {

        this.loading = loading;

    }

    public setUsuarios(usuarios: Usuario[]): void {

        this.usuarios = usuarios;

    }

}
